import 'package:bisa_release/shared/style/styles.dart';
import 'package:bisa_release/shared/widget/share.dart';
import 'package:flutter/material.dart';
import '../../../shared/temp/Quizdata.dart';

class quizSection extends StatefulWidget {
  const quizSection({super.key});

  @override
  State<quizSection> createState() => _quizSectionState();
}

class _quizSectionState extends State<quizSection> {
  final dummyData = Quizdata.getListData();
  final List<bool> _bookmarks = List.filled(10, false);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView.builder(
      physics: BouncingScrollPhysics(),
      itemCount: 10,
      itemBuilder: (context, index) {
        return Container(
          child: Padding(
            padding:
                const EdgeInsets.only(top: 8.0, left: 20, right: 20, bottom: 8),
            child: Container(
              height: 100,
              width: 100,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Styles.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 1,
                    blurRadius: 5,
                    offset: const Offset(0, 2), // changes position of shadow
                  ),
                ],
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, left: 8.0),
                    child: Container(
                      height: 85,
                      width: 120,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Styles.grey,
                        image: DecorationImage(
                            image: AssetImage("${dummyData[index]['image']}"),
                            fit: BoxFit.cover),
                      ),
                      child: Column(
                        children: [Container()],
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0, left: 10.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "${dummyData[index]['title']}",
                          style: Styles.txt16semibold
                              .copyWith(color: Styles.black, fontSize: 14),
                        ),
                        Text(
                          "${dummyData[index]['status']} • ${dummyData[index]['soal']}",
                          style: Styles.txt10semibold,
                        ),
                        Row(
                          children: [
                            shareWidget().FiveStar(14),
                            Text(
                              "${dummyData[index]['rating']}",
                              style: Styles.txt8medium
                                  .copyWith(color: Styles.grey),
                            )
                          ],
                        ),
                        Spacer(),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 8.0),
                          child: Text(
                            "Lihat Comment",
                            style:
                                Styles.txt8medium.copyWith(color: Styles.grey),
                          ),
                        )
                      ],
                    ),
                  ),
                  Spacer(),
                  Padding(
                    padding: const EdgeInsets.only(right: 8, top: 48),
                    child: Container(
                        width: 40,
                        height: 40,
                        decoration: BoxDecoration(
                            color: Styles.white,
                            borderRadius: BorderRadius.circular(5),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black.withOpacity(0.25),
                                spreadRadius: 0,
                                blurRadius: 4,
                                offset:
                                    Offset(0, 4), // changes position of shadow
                              ),
                            ]),
                        child: GestureDetector(
                          onTap: () {
                            setState(() {
                              _bookmarks[index] =
                                  !_bookmarks[index]; // Toggle bookmark state
                            });
                          },
                          child: Icon(
                            _bookmarks[index]
                                ? Icons.bookmark
                                : Icons.bookmark_border_rounded,
                            color: Styles.darkBlue,
                            size: 32,
                          ),
                        )),
                  )
                ],
              ),
            ),
          ),
        );
      },
    ));
  }
}
