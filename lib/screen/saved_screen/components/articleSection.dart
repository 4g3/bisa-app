import 'package:bisa_release/shared/style/styles.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';

class articleSection extends StatefulWidget {
  const articleSection({super.key});

  @override
  State<articleSection> createState() => articleSectionState();
}

class articleSectionState extends State<articleSection> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 130.0),
          child: Image.asset(
            'assets/images/notFound.png',
            height: 200,
            width: 200,
          ),
        ),
        Gap(16),
        Text(
          "Wah , Belum ada artikel yang disimpan !",
          style: Styles.txt16semibold.copyWith(fontSize: 14.5),
        ),
      ],
    );
  }
}
