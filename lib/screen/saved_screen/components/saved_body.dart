import 'package:bisa_release/screen/saved_screen/components/articleSection.dart';
import 'package:bisa_release/screen/saved_screen/components/quizSection.dart';
import 'package:bisa_release/shared/style/styles.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class saved_body extends StatefulWidget {
  const saved_body({Key? key}) : super(key: key);

  @override
  State<saved_body> createState() => _notif_bodyState();
}

class _notif_bodyState extends State<saved_body>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF1CA0E3),
      appBar: AppBar(
        backgroundColor: Color(0xFF1CA0E3),
        elevation: 0,
        title: Text(
          "Disimpan",
          style:
              Styles.txt16semibold.copyWith(color: Colors.white, fontSize: 22),
        ),
        centerTitle: true,
      ),
      body: Container(
        decoration: const BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Color.fromRGBO(0, 0, 0, 0.22),
              offset: Offset(0, 3),
              blurRadius: 4,
            )
          ],
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(25.0),
            topRight: Radius.circular(25.0),
          ),
        ),
        height: MediaQuery.of(context).size.height - 100,
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TabBar(
                controller: _tabController,
                indicator: UnderlineTabIndicator(
                  borderSide: BorderSide(
                    width: 2.0,
                    color: Styles.primaryColor,
                  ),
                  insets: EdgeInsets.symmetric(horizontal: 10),
                ),
                tabs: [
                  Tab(
                    child: Text(
                      'Quiz',
                      style: GoogleFonts.poppins(
                        fontWeight: _tabController.index == 0
                            ? FontWeight.bold
                            : FontWeight.normal,
                        fontSize: _tabController.index == 0 ? 18.0 : 16.0,
                        color: _tabController.index == 0
                            ? Colors.black
                            : Colors.grey,
                      ),
                    ),
                  ),
                  Tab(
                    child: Text(
                      'Artikel',
                      style: GoogleFonts.poppins(
                        fontWeight: _tabController.index == 1
                            ? FontWeight.bold
                            : FontWeight.normal,
                        fontSize: _tabController.index == 1 ? 18.0 : 16.0,
                        color: _tabController.index == 1
                            ? Colors.black
                            : Colors.grey,
                      ),
                    ),
                  ),
                ],
                onTap: (index) {
                  setState(() {
                    _tabController.index = index;
                  });
                },
              ),
            ),
            Expanded(
              child: TabBarView(
                controller: _tabController,
                children: [
                  quizSection(),
                  articleSection(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
