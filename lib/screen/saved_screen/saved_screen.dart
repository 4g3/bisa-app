import 'package:bisa_release/screen/saved_screen/components/saved_body.dart';
import 'package:flutter/material.dart';

class saved_screen extends StatefulWidget {
  const saved_screen({super.key});

  @override
  State<saved_screen> createState() => _saved_screenState();
}

class _saved_screenState extends State<saved_screen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: saved_body());
  }
}
