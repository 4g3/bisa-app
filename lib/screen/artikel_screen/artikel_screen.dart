import 'package:bisa_release/screen/artikel_screen/components/artikel_main.dart';
import 'package:bisa_release/screen/home_screen/home_screen.dart';
import 'package:flutter/material.dart';

import 'package:bisa_release/screen/artikel_screen/components/artikel_title.dart';
import '../../../shared/style/styles.dart';

class artikel_screen extends StatefulWidget {
  const artikel_screen({super.key});

  @override
  State<artikel_screen> createState() => _artikel_screenState();
}

void _showShareMenu(BuildContext context) {
  final RenderBox overlay =
      Overlay.of(context).context.findRenderObject() as RenderBox;

  showMenu(
    context: context,
    position: RelativeRect.fromRect(
      Rect.fromPoints(
        overlay.localToGlobal(overlay.size.bottomRight(Offset(0, 0))),
        overlay.localToGlobal(overlay.size.bottomRight(Offset(0, 0))),
      ),
      Offset.zero & overlay.size,
    ),
    items: [
      PopupMenuItem<int>(
        value: 1,
        child: ListTile(
          leading: Icon(Icons.share),
          title: Text('Share'),
        ),
      ),
      PopupMenuItem<int>(
        value: 2,
        child: ListTile(
          leading: Icon(Icons.tag_faces), // Smile icon
          title: Text('Say Hi'),
        ),
        // Custom behavior when "Say Hi" is pressed
        onTap: () {
          Navigator.pop(context); // Close the popup menu
          _showHiSnackBar(context);
        },
      ),
      // Add more menu items if needed
    ],
  );
}

void _showHiSnackBar(BuildContext context) {
  final snackBar = SnackBar(
    content: Text('Hi Rangga Alrasya🎉'),
    duration: Duration(seconds: 3),
  );
  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}

class _artikel_screenState extends State<artikel_screen> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Scaffold(
            appBar: AppBar(
              backgroundColor: Styles.white,
              leading: GestureDetector(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => HomeScreen()));
                },
                child: IconButton(
                  icon: Image.asset(
                    "assets/images/icon/back-ico.png",
                    color: Styles.black,
                    height: 28,
                  ),
                  onPressed: () => Navigator.of(context).pop(),
                ),
              ),
              centerTitle: true,
              title: Text(
                "Artikel",
                style: Styles.txt24medium.copyWith(color: Styles.black),
              ),
              actions: [
                Builder(
                  builder: (BuildContext context) {
                    return GestureDetector(
                      onTap: () => _showShareMenu(
                          context), // Call the share menu function
                      child: Padding(
                        padding: const EdgeInsets.only(right: 16),
                        child: Icon(
                          Icons.more_vert,
                          color: Styles.black,
                          size: 28,
                        ),
                      ),
                    );
                  },
                ),
              ],
// Function to show the share options me
              elevation: 0,
            ),
            resizeToAvoidBottomInset: true,
            body: SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              child: SafeArea(
                child: SingleChildScrollView(
                    child: Column(
                  children: [
                    SizedBox(
                      height: 8,
                    ),
                    artikel_title(),
                    artikel_main(),
                  ],
                )),
              ),
            )));
  }
}
