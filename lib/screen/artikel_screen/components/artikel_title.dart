import 'package:bisa_release/shared/style/styles.dart';
import 'package:flutter/material.dart';

class artikel_title extends StatefulWidget {
  const artikel_title({super.key});

  @override
  State<artikel_title> createState() => _artikel_titleState();
}

class _artikel_titleState extends State<artikel_title> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Center(
            child: Image(
              width: 1400,
              height: 200,
              image: AssetImage('assets/images/artikel1.png'),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10, left: 20, right: 0),
            child: Text(
              "Berpacu dengan Passion:\nMengatasi Tantangan dan Mendapatkan Keberhasilan dalam Menjalani Passion Anda",
              style: Styles.txt20semibold.copyWith(height: 1.35),
            ),
          )
        ],
      ),
    );
  }
}
