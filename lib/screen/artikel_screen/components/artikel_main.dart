import 'package:bisa_release/shared/style/styles.dart';
import 'package:flutter/material.dart';

class artikel_main extends StatefulWidget {
  const artikel_main({super.key});

  @override
  State<artikel_main> createState() => _artikel_mainState();
}

class _artikel_mainState extends State<artikel_main> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 0),
            child: Row(
              children: [
                Container(
                  width: 18,
                  height: 18,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      color: Styles.grey),
                ),
                SizedBox(width: 4),
                Text(
                  "Admin BISA",
                  style: Styles.txt12medium.copyWith(color: Styles.grey),
                ),
                SizedBox(
                  width: 4,
                ),
                Text(
                  "•",
                  style:
                      Styles.txt24medium.copyWith(color: Styles.primaryColor),
                ),
                SizedBox(
                  width: 4,
                ),
                Icon(
                  Icons.access_time,
                  color: Styles.primaryColor,
                ),
                SizedBox(
                  width: 4,
                ),
                Text(
                  "12 Jam yang lalu",
                  style:
                      Styles.txt12medium.copyWith(color: Styles.primaryColor),
                )
              ],
            ),
          ),
          SizedBox(height: 20),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 40),
            child: Container(
                child: Align(
              alignment: Alignment.topLeft,
              child: Column(
                children: [
                  Text(
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",
                    style: Styles.txt14medium.copyWith(),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Text(
                    "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur",
                    style: Styles.txt14medium.copyWith(),
                  ),
                ],
              ),
            )),
          ),
        ],
      ),
    );
  }
}
