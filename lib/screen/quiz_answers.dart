import 'package:bisa_release/screen/quiztest_screen/quiztest_screen.dart';
import 'package:bisa_release/shared/style/styles.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

import '../../model/questionModel.dart';

class AnswerPage extends StatefulWidget {
  final List<String> selectedAnswers;

  AnswerPage({required this.selectedAnswers});

  @override
  State<AnswerPage> createState() => _AnswerPageState();
}

class _AnswerPageState extends State<AnswerPage> {
  int selectedAnswerIndex = -1; // Initially, no answer is selected
  int currentQuestionIndex = 0; // Track the current question index

  List<Data> questions = [];
  bool isLoading = true;

  @override
  void initState() {
    super.initState();
    fetchQuestion().then((_) {
      setState(() {
        isLoading = false;
      });
    });
  }

  Future<void> fetchQuestion() async {
    // Fetch your question data here using http or any other method
    final response = await http.get(
      Uri.parse('https://bisatrackapp.com/api/questions'),
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ${"L063oh5qw3uHqxHwXc6KjGYF35tB1aBw485p9HnE"}',
      },
    );

    if (response.statusCode == 200) {
      final List<dynamic> jsonData = json.decode(response.body)['data'];
      setState(() {
        questions = jsonData
            .map((e) => Data(
                  id: e['id'],
                  quizId: e['quizId'],
                  text: e['text'],
                  optionA: e['option_a'],
                  optionB: e['option_b'],
                ))
            .toList();
        isLoading = false; // Set isLoading to false after fetching data
      });
      print(response.body);
    } else {
      // Handle the error case
      print('Failed to load data');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(children: [
      Image.asset(
        "assets/images/bg/result.png",
        height: 350,
        width: MediaQuery.of(context).size.width,
        fit: BoxFit.fill,
      ),
      GestureDetector(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => quiztest()),
          );
        },
        child: Padding(
          padding: const EdgeInsets.only(top: 32.0, left: 0),
          child: Row(
            children: [
              IconButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => quiztest()),
                  );
                },
                icon: Image.asset(
                  "assets/images/icon/back-ico.png",
                  height: 32,
                ),
              ),
              Gap(MediaQuery.of(context).size.width * 0.30),
              Image.asset(
                "assets/images/back_loginWhite.png",
                height: 32,
              ),
              Gap(100),
              IconButton(
                  icon: Icon(
                    Icons.info_outline_rounded,
                    color: Styles.white,
                    size: 30,
                  ),
                  onPressed: () {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: Text("Tingkat Keakuratan ",
                                style: Styles.txt16semibold
                                    .copyWith(fontSize: 16)),
                            content: Text(
                              "Tingkat keakuratan dari hasil tes ini belum 100% , jika ingin hasil yang lebih akurat, silahkan lakukan kontak pada psikolog kami.",
                            ),
                            actions: [
                              TextButton(
                                onPressed: () {
                                  Navigator.of(context)
                                      .pop(); // Close the dialog
                                },
                                child: Text("OK"),
                              ),
                            ],
                          );
                        });
                  }),
            ],
          ),
        ),
      ),
      Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 90),
            child: Center(
              child: Container(
                width: 180, // Adjust the width as needed
                height: 180, // Adjust the height as needed
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Color.fromARGB(255, 203, 238, 255),
                  border: Border.all(
                    color: Styles.primaryColor,
                    width: 1,
                  ),
                ),
                child: Center(
                  child: Container(
                    width: 150, // Adjust the width as needed
                    height: 150, // Adjust the height as needed
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Color.fromARGB(255, 161, 204, 240),
                      border: Border.all(
                        color: Styles.primaryColor,
                        width: 1,
                      ),
                    ),
                    child: Center(
                      child: Container(
                        width: 120, // Adjust the width as needed
                        height: 120, // Adjust the height as needed
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Styles.white, // Change the color as needed
                          border: Border.all(
                            color: Styles.primaryColor,
                            width: 1,
                          ),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.only(
                              top: 30.0, left: 8.0, right: 8.0),
                          child: Column(
                            children: [
                              Text('MBTI anda adalah',
                                  textAlign: TextAlign.center,
                                  style: Styles.txt10semibold
                                      .copyWith(color: Styles.primaryColor)),
                              Text('ENTP',
                                  textAlign: TextAlign.center,
                                  style: Styles.txt20bold.copyWith(
                                      color: Styles.primaryColor,
                                      fontSize: 32)),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
          Gap(24),
          Container(
            width: MediaQuery.of(context).size.width * 0.85,
            height: 150,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Styles.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 0,
                  blurRadius: 10,
                  offset: Offset(0, 3), // changes position of shadow
                ),
              ],
            ),
            child: Padding(
              padding: const EdgeInsets.only(top: 5.0, left: 28.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "•",
                            style: Styles.txt24extrabold.copyWith(
                                color: Styles.primaryColor, fontSize: 30),
                          ),
                          Gap(2),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 8.0),
                                child: Text(
                                  "100%",
                                  style: Styles.txt10semibold.copyWith(
                                      color: Styles.primaryColor, fontSize: 18),
                                ),
                              ),
                              Text(
                                "Penyelesaian",
                                style: Styles.txt10regular.copyWith(
                                    color: Styles.black, fontSize: 14),
                              ),
                            ],
                          ),
                        ],
                      ),
                      Gap(14),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "•",
                            style: Styles.txt24extrabold
                                .copyWith(color: Colors.green, fontSize: 30),
                          ),
                          Gap(2),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 8.0),
                                child: Text(
                                  "20",
                                  style: Styles.txt10semibold.copyWith(
                                      color: Colors.green, fontSize: 18),
                                ),
                              ),
                              Text(
                                "Pilihan A",
                                style: Styles.txt10regular.copyWith(
                                    color: Styles.black, fontSize: 14),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                  Gap(24),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "•",
                            style: Styles.txt24extrabold.copyWith(
                                color: Styles.primaryColor, fontSize: 30),
                          ),
                          Gap(2),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 8.0),
                                child: Text(
                                  "33",
                                  style: Styles.txt10semibold.copyWith(
                                      color: Styles.primaryColor, fontSize: 18),
                                ),
                              ),
                              Text(
                                "Total Soal",
                                style: Styles.txt10regular.copyWith(
                                    color: Styles.black, fontSize: 14),
                              ),
                            ],
                          ),
                        ],
                      ),
                      Gap(14),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "•",
                            style: Styles.txt24extrabold
                                .copyWith(color: Colors.yellow, fontSize: 30),
                          ),
                          Gap(2),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 8.0),
                                child: Text(
                                  "13",
                                  style: Styles.txt10semibold.copyWith(
                                      color: Colors.yellow, fontSize: 18),
                                ),
                              ),
                              Text(
                                "Pilhan B",
                                style: Styles.txt10regular.copyWith(
                                    color: Styles.black, fontSize: 14),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Gap(16),
          Text(
            "📋 Review Jawabanmu",
            style: Styles.txt16semibold,
          ),
          Gap(8),
          Container(
            height: 320,
            width: MediaQuery.of(context).size.width * 0.9,
            child: isLoading
                ? Center(child: CircularProgressIndicator())
                : ListView.builder(
                    itemCount: widget.selectedAnswers.length,
                    itemBuilder: (context, index) {
                      final selectedOption =
                          widget.selectedAnswers[index].endsWith('A')
                              ? 'A'
                              : 'B';
                      final optionToShow = selectedOption == 'A'
                          ? questions[index].optionA
                          : questions[index].optionB;
                      return ListTile(
                        title: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(top: 0),
                              child: Text(
                                index < questions.length
                                    ? questions[index].text ??
                                        'No question available'
                                    : 'No question available',
                                style: Styles.txt16semibold
                                    .copyWith(color: Styles.primaryColor),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ],
                        ),
                        // Display the selected answer for this question
                        subtitle: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text(
                              'Jawaban Anda : ',
                              //${widget.selectedAnswers[index]}',
                              style: Styles.txt14regular
                                  .copyWith(color: Colors.black),
                            ),
                            Text("$optionToShow",
                                style: Styles.txt14semibold
                                    .copyWith(fontSize: 13)),
                            Gap(8)
                          ],
                        ),
                      );
                    },
                  ),
          ),
          Gap(8)
        ],
      )
    ]));
  }
}
