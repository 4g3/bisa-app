import 'package:bisa_release/screen/register_screen/register_screen.dart';
import 'package:bisa_release/shared/widget/bottom_nav.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:shared_preferences/shared_preferences.dart';

class load_Screen extends StatefulWidget {
  const load_Screen({Key? key}) : super(key: key);

  @override
  _load_ScreenState createState() => _load_ScreenState();
}

class _load_ScreenState extends State<load_Screen> {
  @override
  void initState() {
    super.initState();

    // Add a timer to determine where to navigate the user
    Future.delayed(Duration(seconds: 4), () {
      checkUserLoginStatus();
    });
  }

  Future<void> checkUserLoginStatus() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String? token = pref.getString('token');
    String? username = pref.getString('username');

    print('token: $token');
    print('username: $username');

    if (token != null && username != null) {
      // User has logged in before, navigate to home screen with stored data
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => BottomNavigationBisaApp(),
        ),
      );
    } else {
      // User is accessing the app for the first time or hasn't logged in,
      // navigate to the registration screen
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => RegisterScreen(),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            Gap(MediaQuery.of(context).size.height * 0.4),
            Image.asset(
              'assets/images/icon/loadLogo.png',
              width: 140,
            ),
            Gap(6),
            Image.asset(
              'assets/images/back_login.png',
              width: 100,
            ),
          ],
        ),
      ),
    );
  }
}
