import 'package:bisa_release/shared/style/styles.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';

class setting_title extends StatefulWidget {
  const setting_title({super.key});

  @override
  State<setting_title> createState() => _setting_titleState();
}

class _setting_titleState extends State<setting_title> {
  final TextEditingController _searchController = TextEditingController();
  List<SettingItem> allSettings = [
    SettingItem('Akun', Icons.person_outline_rounded),
    SettingItem('Setting Notifikasi', Icons.notifications_active_outlined),
    SettingItem('Privacy & Policy', Icons.privacy_tip_outlined),
    SettingItem('Keamanan', Icons.security_outlined),
    SettingItem('Pengaturan Umum', Icons.settings_outlined),
    SettingItem('Izin Aplikasi', Icons.app_settings_alt_outlined),
    SettingItem('Social Media Kami', Icons.groups_outlined),
  ];

  List<SettingItem> displayedSettings = [];

  @override
  void initState() {
    super.initState();
    displayedSettings = allSettings;
    _searchController.addListener(() {
      filterSearchResults(_searchController.text);
    });
  }

  void filterSearchResults(String query) {
    List<SettingItem> searchResult = allSettings.where((setting) {
      return setting.title.toLowerCase().contains(query.toLowerCase());
    }).toList();

    setState(() {
      displayedSettings = searchResult;
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus(); //to hide the keyboard - if any
      },
      child: CustomScrollView(
        physics: BouncingScrollPhysics(),
        slivers: <Widget>[
          SliverAppBar(
            backgroundColor: Color(0xffFAFAFA),
            elevation: 0,
            expandedHeight: 250.0,
            floating: false,
            pinned: true,
            onStretchTrigger: () {
              return Future<void>.value();
            },
            stretch: true,
            flexibleSpace: LayoutBuilder(
              builder: (BuildContext context, BoxConstraints constraints) {
                final bool isExpanded = constraints.maxHeight >
                    (200 + MediaQuery.of(context).padding.top);
                final double paddingValue = isExpanded ? 80.0 : 0.0;
                final double titleTopPadding = isExpanded ? 50.0 : 0.0;

                return FlexibleSpaceBar(
                  titlePadding: EdgeInsets.only(
                    bottom: paddingValue,
                    top: titleTopPadding,
                    left: 50.0,
                  ),
                  title: Text(
                    'Setting',
                    style: Styles.txt24medium
                        .copyWith(color: Styles.black, fontSize: 22),
                  ),
                );
              },
            ),
          ),
          SliverAppBar(
            backgroundColor: Color.fromARGB(255, 250, 250, 250),
            elevation: 0,
            pinned: true,
            automaticallyImplyLeading: false,
            title: Padding(
              padding: const EdgeInsets.only(
                  top: 8, left: 16.0, right: 16, bottom: 16),
              child: SearchBar(controller: _searchController),
            ),
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.only(top: .0, left: 16.0, right: 16.0),
              child: Column(
                children: [
                  displayedSettings.isEmpty
                      ? Padding(
                          padding:
                              const EdgeInsets.only(top: 100.0, bottom: 80),
                          child: Center(
                            child: Text("Setting tidak ditemukan",
                                style: Styles.txt16regular),
                          ),
                        )
                      : ListView.builder(
                          shrinkWrap: true,
                          physics: BouncingScrollPhysics(), // Add this line
                          itemCount: displayedSettings.length,
                          itemBuilder: (context, index) {
                            return ListTile(
                              leading: Icon(displayedSettings[index].icon),
                              title: Row(
                                children: [
                                  Text(displayedSettings[index].title,
                                      style: Styles.txt16medium),
                                  Spacer(),
                                  Icon(Icons.arrow_forward_ios_rounded,
                                      size: 24),
                                ],
                              ),
                              onTap: () {
                                // Handle tapping on a setting item
                                // You can navigate to a detail screen or perform other actions.
                              },
                            );
                          },
                        ),
                  Container(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: [
                          Gap(6),
                          Image.asset(
                            'assets/images/icon/loadLogo.png',
                            width: 100,
                          ),
                          Gap(2),
                          Text(
                            "BISA App",
                            style: Styles.txt20bold,
                          ),
                          Gap(0),
                          Text(
                            "© Copyright 2023 , BISA team",
                            style: Styles.txt12medium,
                          ),
                          SizedBox(
                            height: 20,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class SearchBar extends StatelessWidget {
  final TextEditingController controller;

  SearchBar({required this.controller});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        height: 50,
        width: MediaQuery.of(context).size.width * 0.85,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4),
          color: Color.fromARGB(255, 229, 229, 229),
        ),
        child: TextField(
          controller: controller,
          onTap: () {},
          decoration: InputDecoration(
            hintText: "Cari di Setting",
            suffixIcon: GestureDetector(
              onTap: () {
                // Start voice recognition when the microphone icon is tapped
                // Add your voice recognition logic here
              },
              child: Icon(
                Icons.mic_rounded,
                color: Colors.grey,
              ),
            ),
            hintStyle:
                Styles.txt14medium.copyWith(color: Colors.grey, fontSize: 12),
            prefixIcon: Icon(
              Icons.search_rounded,
              color: Colors.grey,
            ),
            filled: true,
            fillColor: const Color.fromARGB(0, 255, 255, 255),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(100),
              borderSide: BorderSide.none,
            ),
          ),
        ),
      ),
    );
  }
}

class SettingItem {
  final String title;
  final IconData icon;

  SettingItem(this.title, this.icon);
}
