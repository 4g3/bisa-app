import 'package:bisa_release/shared/style/styles.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';

class ForYouExploreScreen extends StatelessWidget {
  const ForYouExploreScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("Pekerjaan paling banyak dicari", style: Styles.txt20medium,),
        SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          scrollDirection: Axis.horizontal,
          child: Row(
            children: [
              Image.asset("assets/images/icon/dm-ico.png", width: 100,),
              Image.asset("assets/images/icon/content-ico.png", width: 100,),
              Image.asset("assets/images/icon/videoedit-ico.png", width: 100,),
              Image.asset("assets/images/icon/software-ico.png", width: 100,),
              Image.asset("assets/images/icon/data-ico.png", width: 100,),
            ],
          ),
        ),
        Gap(16),
        Text("Semua Pekerjaan", style: Styles.txt20medium,)
      ],
    );
  }
}
