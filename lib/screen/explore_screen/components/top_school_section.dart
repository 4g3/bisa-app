import 'package:flutter/material.dart';
import 'package:gap/gap.dart';

import '../../../shared/temp/TopSchooldata.dart';
import '../../../shared/style/styles.dart';

class TopSchoolExploreScreen extends StatelessWidget {
  const TopSchoolExploreScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final List<Map<String, dynamic>> topSchools = TopSchoolData.getListData();

    return Expanded(
      child: Column(
        children: [
          ListView.builder(
            itemCount: topSchools.length,
            shrinkWrap: true,
            physics: ClampingScrollPhysics(),
            itemBuilder: (context, index) {
              final school = topSchools[index];
              final String titleSc = school['titleSc'];
              final String addressSc = school['addressSc'];
              final String jurSc =
                  school['jurSc'].join(' - '); // Combine jurSc values
              final String jarakSc = school['jarakSc'];
              final String ratingSc = school['ratingSc'];
              final String imgSc = school['imgSc']; // Get image path

              return Container(
                margin: EdgeInsets.symmetric(horizontal: 16).copyWith(top: 16),
                width: double.infinity,
                padding: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(8),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(0.25),
                      blurRadius: 15,
                      offset: Offset(0, 4), // changes position of shadow
                    ),
                  ],
                ),
                child: Row(
                  children: [
                    Container(
                      width: 80,
                      height: 100,
                      decoration: BoxDecoration(
                        color: Styles.primaryColor,
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: Image.asset(
                        imgSc, // Use the image path
                        fit: BoxFit.cover,
                      ),
                    ),
                    Gap(10),
                    Container(
                      width: 206,
                      child: Stack(
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(titleSc, style: Styles.txt14semibold),
                              Text(
                                addressSc,
                                style: Styles.txt10medium
                                    .copyWith(color: Styles.grey),
                              ),
                              Gap(4),
                              Row(
                                children: [
                                  Icon(
                                    Icons.menu_book_rounded,
                                    size: 18,
                                    color: Styles.black,
                                  ),
                                  Gap(4),
                                  Text(
                                    jurSc,
                                    style: Styles.txt12medium
                                        .copyWith(color: Styles.grey),
                                  ),
                                ],
                              ),
                              // Row for distance
                              Row(
                                children: [
                                  Icon(
                                    Icons.map_rounded,
                                    size: 18,
                                    color: Styles.black,
                                  ),
                                  Gap(4),
                                  Text(
                                    jarakSc,
                                    style: Styles.txt12medium
                                        .copyWith(color: Styles.grey),
                                  ),
                                ],
                              ),
                              Row(
                                children: [
                                  Icon(
                                    Icons.star,
                                    size: 18,
                                    color: Styles.black,
                                  ),
                                  Gap(4),
                                  Text(
                                    ratingSc,
                                    style: Styles.txt12medium
                                        .copyWith(color: Styles.grey),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          Positioned(
                            top: 65,
                            left: 106,
                            child: Container(
                              alignment: Alignment.center,
                              width: 100,
                              height: 30,
                              padding: EdgeInsets.symmetric(
                                  horizontal: 8, vertical: 4),
                              decoration: BoxDecoration(
                                color: Styles.primaryColor,
                                borderRadius: BorderRadius.circular(8),
                              ),
                              child: Text(
                                "Lokasi & Detail",
                                style: Styles.txt10medium
                                    .copyWith(color: Colors.white),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
