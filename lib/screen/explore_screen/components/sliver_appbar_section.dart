import 'package:flutter/material.dart';

import '../../../shared/style/styles.dart';

class SliverAppBarExploreScreen extends StatefulWidget {
  const SliverAppBarExploreScreen({super.key});

  @override
  State<SliverAppBarExploreScreen> createState() => _SliverAppBarExploreScreenState();
}

class _SliverAppBarExploreScreenState extends State<SliverAppBarExploreScreen> {
  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
        automaticallyImplyLeading: false,
        backgroundColor:Styles.primaryColor,
        collapsedHeight: 60,
        expandedHeight: 240,
        floating: false,
        pinned: true,
        flexibleSpace: FlexibleSpaceBar(
            titlePadding: EdgeInsets.only(bottom: 2),
            centerTitle: true,
            expandedTitleScale: 1.05,
            //title itu buat search bar nya
            title: Container(
              margin: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
              width: double.infinity,
              height: 41,
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(0.25),
                      spreadRadius: 0,
                      blurRadius: 8,
                      offset: Offset(0, 1), // changes position of shadow
                    ),
                  ]
              ),
              child: TextField(
                onTap: () {},
                decoration: InputDecoration(
                  hintText: "Cari Peluang Karier Luar Biasa!",
                  suffixIcon: Icon(
                    Icons.mic_rounded,
                    color: Styles.grey,
                  ),
                  hintStyle: Styles.txt14regular.copyWith(color: Color(0xFFC1C1C1),height: 1),
                  prefixIcon: Icon(
                    Icons.search_rounded,
                    color: Styles.grey,
                  ),
                  filled: true,
                  fillColor: Colors.white,
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(100),
                    borderSide: BorderSide.none,
                  ),
                ),
              ),
            ),
            background: Container(
              alignment: Alignment.bottomLeft,
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 10).copyWith(bottom: 70),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text("Inspirasikan Kariermu", style: Styles.txt24semibold.copyWith(color: Colors.white),),
                  Container(
                    child: Text("Temukan Peluang Karier yang Menarik untuk Menggapai Impianmu!", style: Styles.txt14regular.copyWith(color: Colors.white),),
                  )
                ],
              ),
            )));
  }
}
