import 'package:bisa_release/shared/style/styles.dart';
import 'package:flutter/material.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class TrendExploreScreen extends StatefulWidget {
  const TrendExploreScreen({super.key});

  @override
  _TrendExploreScreenState createState() => _TrendExploreScreenState();
}

class _TrendExploreScreenState extends State<TrendExploreScreen> {
  final List<String> videoUrls = [
    'https://youtu.be/1trtyvc0r1s',
    'https://youtu.be/9VHCA1tvhEY',
    'https://youtu.be/ugz0ttPYXF0',
    'https://youtu.be/j2fWj5bXbmo',
  ];

  late List<YoutubePlayerController> _controllers;
  late List<bool> _isVideoLoaded;

  @override
  void initState() {
    super.initState();
    _controllers = List.generate(
      videoUrls.length,
      (index) => YoutubePlayerController(
        initialVideoId: YoutubePlayer.convertUrlToId(videoUrls[index])!,
        flags: YoutubePlayerFlags(
          autoPlay: false,
          mute: false,
        ),
      ),
    );
    _isVideoLoaded = List.generate(videoUrls.length, (index) => false);
  }

  @override
  void dispose() {
    for (var controller in _controllers) {
      controller.dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.only(bottom: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Konten edukasi Pilihan kami", style: Styles.txt20medium),
            ListView.builder(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              itemCount: videoUrls.length,
              itemBuilder: (context, index) {
                return GestureDetector(
                  onTap: () {
                    setState(() {
                      // Set the video to loaded when tapped
                      _isVideoLoaded[index] = true;
                    });
                  },
                  child: Container(
                    margin: EdgeInsets.only(top: 16),
                    width: double.infinity,
                    height: 200,
                    decoration: BoxDecoration(
                      color: Styles.primaryColor,
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: !_isVideoLoaded[index]
                        ? Center(
                            child: Icon(
                              Icons.play_arrow,
                              size: 48,
                              color: Colors.white,
                            ),
                          )
                        : YoutubePlayer(
                            controller: _controllers[index],
                            showVideoProgressIndicator: true,
                            progressIndicatorColor: Colors.blueAccent,
                          ),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
