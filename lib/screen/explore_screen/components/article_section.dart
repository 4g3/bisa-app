import 'package:bisa_release/shared/style/styles.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';

class ArticleExploreScreen extends StatelessWidget {
  const ArticleExploreScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ListView.separated(
          itemCount: 6,
          shrinkWrap: true,
          physics: ClampingScrollPhysics(),
          separatorBuilder: (context, index) => Divider(
            thickness: 1,
          ),
          itemBuilder: (context, index) {
            return Column(
              children: [
                Gap(4),
                Row(
                  children: [
                    Image.asset(
                      "assets/images/epos.png",
                      width: 130,
                      height: 80,
                    ),
                    Gap(4),
                    Container(
                      width: 170,
                      child: Text(
                        "Mengejar Passion: Panduan Praktis untuk Menemukan dan Mengikuti Gairah Anda",
                        style: Styles.txt12medium,
                      ),
                    ),
                  ],
                ),
                Gap(4),
              ],
            );
          },
        ),
      ],
    );
  }
}
