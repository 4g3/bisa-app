import 'package:bisa_release/screen/explore_screen/components/article_section.dart';
import 'package:bisa_release/screen/explore_screen/components/sliver_appbar_section.dart';
import 'package:bisa_release/screen/explore_screen/components/top_school_section.dart';
import 'package:bisa_release/screen/explore_screen/components/trend_section.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';

import '../../shared/style/styles.dart';

class ExploreScreen extends StatefulWidget {
  const ExploreScreen({super.key});

  @override
  State<ExploreScreen> createState() => _ExploreScreenState();
}

class _ExploreScreenState extends State<ExploreScreen>
    with SingleTickerProviderStateMixin {
  TabController? _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(
      length: 3,
      vsync: this,
    );
  }

  @override
  void dispose() {
    _tabController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SafeArea(
          child: NestedScrollView(
              headerSliverBuilder:
                  (BuildContext context, bool innerBoxIsScrolled) {
                return <Widget>[
                  SliverAppBarExploreScreen(),
                ];
              },
              body: SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  child: Column(
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(
                            8.0), // Adjust the radius as needed
                        child: Container(
                          width: 30.0, // Width of 10 units
                          height: 4.0, // Height of 6 units
                          color: Colors.grey,
                        ),
                      ),
                      Gap(8),
                      Container(
                        child: TabBar(
                          physics: BouncingScrollPhysics(),
                          isScrollable: true,
                          controller: _tabController,
                          indicatorColor: Styles.primaryColor,
                          indicatorSize: TabBarIndicatorSize.label,
                          labelColor: Styles.black,
                          labelStyle: Styles.txt20semibold,
                          unselectedLabelColor: Styles.grey,
                          unselectedLabelStyle: Styles.txt20semibold,
                          tabs: [
                            Tab(
                              text: "Trend",
                            ),
                            Tab(
                              text: "Article",
                            ),
                            Tab(
                              text: "Top School",
                            ),
                          ],
                        ),
                      ),
                      ClipRRect(
                        borderRadius: BorderRadius.circular(8.0),
                        child: Container(
                          padding: EdgeInsets.only(top: 16),
                          height: MediaQuery.of(context).size.height,
                          child: TabBarView(
                            physics: BouncingScrollPhysics(),
                            controller: _tabController,
                            children: [
                              Padding(
                                padding: EdgeInsets.symmetric(horizontal: 24.0),
                                child: TrendExploreScreen(),
                              ),
                              Padding(
                                padding: EdgeInsets.symmetric(horizontal: 24.0),
                                child: ArticleExploreScreen(),
                              ),
                              TopSchoolExploreScreen(),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              )),
        ),
      ),
    );
  }
}
