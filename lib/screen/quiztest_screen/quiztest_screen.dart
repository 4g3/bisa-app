import 'package:bisa_release/screen/quiztest_screen/components/quiztest_body.dart';
import 'package:bisa_release/shared/style/styles.dart';
import 'package:bisa_release/shared/widget/bottom_nav.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';

class quiztest extends StatefulWidget {
  const quiztest({super.key});

  @override
  State<quiztest> createState() => quiztestState();
}

class quiztestState extends State<quiztest> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: const Color.fromARGB(0, 255, 255, 255),
          elevation: 0,
          leading: IconButton(
            icon: Image.asset(
              "assets/images/icon/back-ico.png",
              height: 25,
              color: Colors.black,
            ),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => BottomNavigationBisaApp()));
            },
          ),
          actions: [
            IconButton(
                icon: Icon(
                  Icons.info_outline_rounded,
                  color: Colors.black,
                  size: 30,
                ),
                onPressed: () {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          title: Text("Tingkat Keakuratan ⭐",
                              style:
                                  Styles.txt16semibold.copyWith(fontSize: 16)),
                          content: Text(
                            "Angka 4.0 disebelah logo bintang menggambarkan tingkat keakuratan dari hasil tes ini. Semakin tinggi angkanya, semakin akurat hasilnya.",
                          ),
                          actions: [
                            TextButton(
                              onPressed: () {
                                Navigator.of(context).pop(); // Close the dialog
                              },
                              child: Text("OK"),
                            ),
                          ],
                        );
                      });
                }),
            Gap(4),
          ],
          centerTitle: false,
        ),
        body: quiztest_body());
  }
}
