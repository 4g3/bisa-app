import 'package:bisa_release/screen/quiz_detail/quiz_detail.dart';
import 'package:bisa_release/shared/style/styles.dart';
import 'package:bisa_release/shared/widget/share.dart';
import 'package:expandable_text/expandable_text.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'dart:math' as math;

class quiztest_body extends StatefulWidget {
  const quiztest_body({super.key});

  @override
  State<quiztest_body> createState() => _quiztest_bodyState();
}

class _quiztest_bodyState extends State<quiztest_body> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 20.0, left: 290),
                  child: Image.asset(
                    "assets/images/bg/art1.png",
                    fit: BoxFit.cover,
                    width: 60,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 160.0, left: 250),
                  child: Image.asset(
                    "assets/images/bg/art2.png",
                    fit: BoxFit.fill,
                    height: 130,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 180.0, left: 10),
                  child: Image.asset(
                    "assets/images/bg/art3.png",
                    fit: BoxFit.fill,
                    height: 100,
                  ),
                ),
                Container(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 30, top: 0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "MBTI Test",
                          style: Styles.txt20semibold.copyWith(fontSize: 30),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.content_paste_outlined,
                              size: 21,
                              color: Styles.grey,
                            ),
                            Gap(6),
                            Text(
                              "BISA Team",
                              style: Styles.txt12medium
                                  .copyWith(color: Styles.grey),
                            ),
                            Gap(6),
                            Container(
                              width: 6,
                              height: 6,
                              decoration: BoxDecoration(
                                color: Styles.grey,
                                borderRadius: BorderRadius.circular(12),
                              ),
                            ),
                            Gap(6),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                shareWidget().FiveStar(14),
                                Gap(6),
                                Text(
                                  "4.0",
                                  style: Styles.txt20regular.copyWith(
                                      color: Styles.grey, fontSize: 14),
                                ),
                              ],
                            ),
                          ],
                        ),
                        Gap(20),
                        Container(
                          height: 150,
                          width: 300,
                          decoration: BoxDecoration(
                            color: Styles.white,
                            borderRadius: BorderRadius.circular(12),
                            border: Border.all(
                                color: Styles.grey.withOpacity(0.2), width: 1),
                            boxShadow: [
                              BoxShadow(
                                color: Styles.grey.withOpacity(0.2),
                                spreadRadius: 2,
                                blurRadius: 6,
                                offset:
                                    Offset(0, 5), // changes position of shadow
                              ),
                            ],
                          ),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(
                                12), // Clip with the same border radius as the container
                            child: Image.asset(
                              "assets/images/borobudur.png",
                              fit: BoxFit.cover,
                              height: 100,
                            ),
                          ),
                        ),
                        Gap(30),
                        Row(
                          children: [
                            Text(
                              "📌",
                              style: Styles.txt20semibold,
                            ),
                            Gap(8),
                            Text(
                              "Informasi Test",
                              style:
                                  Styles.txt16semibold.copyWith(fontSize: 18),
                            ),
                          ],
                        ),
                        Container(
                          height: 80,
                          width: 300,
                          decoration: BoxDecoration(
                            color: Color.fromARGB(0, 242, 242, 242),
                            borderRadius: BorderRadius.circular(12),
                          ),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image.asset(
                                "assets/images/icon/jumlahSoal.png",
                                height: 52,
                                fit: BoxFit.cover,
                              ),
                              Gap(10),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Jumlah\nSoal",
                                    style: Styles.txt12bold.copyWith(
                                        fontSize: 14, color: Styles.black),
                                  ),
                                  Gap(0),
                                  Text(
                                    "12 Soal",
                                    style: Styles.txt12medium.copyWith(
                                        fontSize: 12, color: Styles.grey),
                                  ),
                                ],
                              ),
                              Gap(16),
                              Container(
                                width: 1,
                                height: 35,
                                // color: Styles.grey.withOpacity(0.2),
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Styles.grey.withOpacity(0.2),
                                        width: 1)),
                              ),
                              Gap(16),
                              Image.asset(
                                "assets/images/icon/waktuSoal.png",
                                height: 47,
                                fit: BoxFit.cover,
                              ),
                              Gap(10),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Waktu\nPengerjaan",
                                    style: Styles.txt12bold.copyWith(
                                        fontSize: 14, color: Styles.black),
                                  ),
                                  Gap(0),
                                  Text(
                                    "± 20 mnt",
                                    style: Styles.txt12medium.copyWith(
                                        fontSize: 12, color: Styles.grey),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        Gap(8),
                        Padding(
                          padding: const EdgeInsets.only(left: 16, right: 48),
                          child: ExpandableText(
                            "Quiz Soal MBTI (Myers-Briggs Type Indicator) adalah sebuah ujian yang dirancang untuk membantu individu memahami dan mengidentifikasi tipe kepribadian mereka berdasarkan preferensi dalam empat dichotomy fundamental, yaitu: Ekstraversi vs. Introversi, Sensorik vs. Intuitif, Berpikir vs. Perasaan, Penilaian vs. Persepsi.",
                            expandText: 'baca selengkapnya',
                            collapseText: 'sembunyikan',
                            maxLines: 6,
                            style: Styles.txt12regular
                                .copyWith(fontSize: 15, color: Styles.grey),
                            linkColor: Colors.blue,
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Gap(135),
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => QuizDetail()));
                          },
                          child: Container(
                            height: 50,
                            width: 300,
                            decoration: BoxDecoration(
                              color: Styles.primaryColor,
                              borderRadius: BorderRadius.circular(12),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                SizedBox(
                                  width: 14,
                                ),
                                TextButton(
                                  onPressed: () {},
                                  child: Text("Mulai Test",
                                      style: Styles.txt10semibold.copyWith(
                                          fontSize: 17, color: Styles.white)),
                                ),
                                Spacer(),
                                Transform(
                                  alignment: Alignment.center,
                                  transform: Matrix4.rotationY(math
                                      .pi), // Rotate 180 degrees (right to left)
                                  child: Image.asset(
                                    "assets/images/icon/back-ico.png",
                                    height: 20,
                                  ),
                                ),
                                SizedBox(
                                  width: 20,
                                ),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
