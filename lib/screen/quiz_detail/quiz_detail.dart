import 'dart:convert';
import 'package:bisa_release/screen/quiz_answers.dart';
import 'package:bisa_release/screen/quiztest_screen/quiztest_screen.dart';
import 'package:bisa_release/shared/style/styles.dart';
import 'package:gap/gap.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

import '../../model/questionModel.dart';

class QuizDetail extends StatefulWidget {
  @override
  _QuizDetailState createState() => _QuizDetailState();
}

class _QuizDetailState extends State<QuizDetail> {
  int selectedAnswerIndex = -1; // Initially, no answer is selected
  int currentQuestionIndex = 0; // Track the current question index

  List<String> selectedAnswers = [];

  List<Data> questions = [];
  bool isLoading = true;

  Future<void> fetchQuestion() async {
    final response = await http
        .get(Uri.parse('https://bisatrackapp.com/api/questions'), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ${"L063oh5qw3uHqxHwXc6KjGYF35tB1aBw485p9HnE"}',
    });

    if (response.statusCode == 200) {
      final List<dynamic> jsonData = json.decode(response.body)['data'];
      Questions.fromJson(json.decode(response.body));
      setState(() {
        questions = jsonData
            .map((e) => Data(
                  id: e['id'],
                  quizId: e['quizId'],
                  text: e['text'],
                  optionA: e['option_a'],
                  optionB: e['option_b'],
                ))
            .toList();
      });
      print(response.body);
    } else {
      ('Failed to load data');
    }
  }

  @override
  void initState() {
    super.initState();
    try {
      fetchQuestion().then((_) {
        setState(() {
          isLoading = false;
        });
      });
    } catch (error) {
      print('Error in initState: $error');
      setState(() {
        isLoading = false; // Juga nonaktifkan loading jika terjadi kesalahan
      });
    }
  }

  double progressPercentage = 50.0; // Initial percentage

  void goToNextQuestion() {
    if (selectedAnswerIndex != -1) {
      // Check if an answer is selected
      selectedAnswers.add(
        currentQuestionIndex.toString() +
            '.' +
            (selectedAnswerIndex == 0 ? 'A' : 'B'),
      );
      if (currentQuestionIndex == questions.length - 1) {
        // If it's the last question, navigate to the answer page
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => AnswerPage(selectedAnswers: selectedAnswers),
          ),
        );
      } else {
        // Move to the next question and clear the selected answer
        setState(() {
          currentQuestionIndex++;
          selectedAnswerIndex = -1;
        });
      }
    } else {
      // Show a message or prompt the user to select an answer before proceeding
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Tolong pilih salah satu sebelum melanjutkan!"),
            actions: <Widget>[
              TextButton(
                child: Text("OK"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    double outer = 220;
    // double innerWidth = (progressPercentage / 100) * outer;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(
            Icons.close_rounded,
            color: Colors.black,
          ),
          onPressed: () {
            showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Text("Peringatan"),
                  content: Text(
                      "Progress anda akan hilang selama mengerjakan yakin keluar?"),
                  actions: <Widget>[
                    TextButton(
                      child: Text("Cancel"),
                      onPressed: () {
                        Navigator.of(context).pop(); // Close the dialog
                      },
                    ),
                    TextButton(
                      child: Text("Confirm"),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => quiztest()));
                      },
                    ),
                  ],
                );
              },
            );
          },
        ),
        title: Row(
          children: [
            Container(
              height: 6,
              width: outer,
              decoration: BoxDecoration(
                color: Color.fromARGB(255, 220, 220, 220),
                borderRadius: BorderRadius.circular(12),
              ),
              child: FractionallySizedBox(
                widthFactor: progressPercentage / 100,
                alignment: Alignment.centerLeft,
                child: Container(
                  height: 6,
                  decoration: BoxDecoration(
                    color: Colors.blue, // Changed to blue for progress
                    borderRadius: BorderRadius.circular(12),
                  ),
                ),
              ),
            ),
            SizedBox(width: 16),
            Expanded(
              // Wrap the Text widget with Expanded
              child: Text(
                "${currentQuestionIndex + 1}/${questions.length}", // Display current question number
                style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
              ),
            )
          ],
        ),
        elevation: 0,
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 0, left: 16, right: 16),
            child: Container(
              child: Column(
                children: [
                  SizedBox(height: 8),
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width,
                          height: 160,
                          decoration: BoxDecoration(
                            color: Color(0xffDCF8FF),
                            borderRadius: BorderRadius.circular(8),
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Text(
                                  currentQuestionIndex < questions.length
                                      ? questions[currentQuestionIndex].text ??
                                          'No question available'
                                      : 'No question available',
                                  style: Styles.txt16semibold,
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 30),
                        Text(
                          "Pilih salah satu yang paling cocok dengan dirimu",
                          style: Styles.txt16semibold,
                          textAlign: TextAlign.left,
                        ),
                        SizedBox(height: 10),
                        Column(
                          children: [
                            Container(
                              child: GestureDetector(
                                onTap: () {
                                  setState(() {
                                    selectedAnswerIndex =
                                        0; // Set to 0 for option A
                                  });
                                },
                                child: Container(
                                  width: MediaQuery.of(context).size.width,
                                  height: 65,
                                  decoration: BoxDecoration(
                                    color: selectedAnswerIndex == 0
                                        ? Color(0xffDCF8FF)
                                        : Color.fromARGB(255, 255, 255, 255),
                                    borderRadius: BorderRadius.circular(8),
                                    border: Border.all(
                                      color: selectedAnswerIndex == 0
                                          ? Color(0xff3EB8D4)
                                          : Colors.black38,
                                      width: 1,
                                    ),
                                  ),
                                  child: Center(
                                    child: Padding(
                                      padding: const EdgeInsets.only(left: 16),
                                      child: Row(
                                        children: [
                                          Container(
                                            width: 265,
                                            child: Text(
                                                currentQuestionIndex <
                                                        questions.length
                                                    ? questions[currentQuestionIndex]
                                                            .optionA ??
                                                        'No option available 1'
                                                    : 'No option available 1',
                                                style: Styles.txt14semibold
                                                    .copyWith(
                                                  color: selectedAnswerIndex ==
                                                          0
                                                      ? Color(0xff3EB8D4)
                                                      : Color.fromARGB(
                                                          255, 129, 129, 129),
                                                ) // Change to your unselected text color),
                                                ),
                                          ),
                                          Spacer(),
                                          selectedAnswerIndex == 0
                                              ? Container(
                                                  child: Icon(
                                                      Icons.check_box_rounded,
                                                      color: Color(0xff3EB8D4)),
                                                )
                                              : Container(
                                                  child: Icon(
                                                      Icons
                                                          .check_box_outline_blank_outlined,
                                                      color: Color.fromARGB(
                                                          255, 129, 129, 129)),
                                                ),
                                          SizedBox(
                                            width: 16,
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Gap(16),
                            Text(
                              "atau",
                              style: Styles.txt16medium.copyWith(
                                  color: Color.fromARGB(255, 129, 129, 129),
                                  letterSpacing: 1.5),
                            ),
                            Gap(16),
                            Container(
                              child: GestureDetector(
                                onTap: () {
                                  setState(() {
                                    selectedAnswerIndex =
                                        1; // Set to 1 for option B
                                  });
                                },
                                child: Container(
                                  width: MediaQuery.of(context).size.width,
                                  height: 60,
                                  decoration: BoxDecoration(
                                    color: selectedAnswerIndex == 1
                                        ? Color(0xffDCF8FF)
                                        : Color.fromARGB(255, 255, 255, 255),
                                    borderRadius: BorderRadius.circular(8),
                                    border: Border.all(
                                      color: selectedAnswerIndex == 1
                                          ? Color(0xff3EB8D4)
                                          : Colors.black38,
                                      width: 1,
                                    ),
                                  ),
                                  child: Center(
                                    child: Padding(
                                      padding: const EdgeInsets.only(left: 16),
                                      child: Row(
                                        children: [
                                          Container(
                                            width: 265,
                                            child: Text(
                                                currentQuestionIndex <
                                                        questions.length
                                                    ? questions[currentQuestionIndex]
                                                            .optionB ??
                                                        'No option available 1'
                                                    : 'No option available 1',
                                                style: Styles.txt14semibold
                                                    .copyWith(
                                                  color: selectedAnswerIndex ==
                                                          1
                                                      ? Color(0xff3EB8D4)
                                                      : Color.fromARGB(
                                                          255, 129, 129, 129),
                                                ) // Change to your unselected text color),
                                                ),
                                          ),
                                          Spacer(),
                                          selectedAnswerIndex == 1
                                              ? Container(
                                                  child: Icon(
                                                      Icons.check_box_rounded,
                                                      color: Color(0xff3EB8D4)),
                                                )
                                              : Container(
                                                  child: Icon(
                                                      Icons
                                                          .check_box_outline_blank_outlined,
                                                      color: Color.fromARGB(
                                                          255, 129, 129, 129)),
                                                ),
                                          SizedBox(
                                            width: 16,
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Spacer(),
          GestureDetector(
            onTap: goToNextQuestion,
            child: Container(
              width: MediaQuery.of(context).size.width * 0.9,
              height: 50,
              decoration: BoxDecoration(
                color: Styles.primaryColor,
                borderRadius: BorderRadius.circular(8),
              ),
              child: Center(
                child: Text(
                  "Lanjutkan",
                  style: Styles.txt16semibold.copyWith(color: Styles.white),
                ),
              ),
            ),
          ),
          SizedBox(height: 20),
        ],
      ),
    );
  }
}
