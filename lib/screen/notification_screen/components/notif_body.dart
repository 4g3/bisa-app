import 'package:bisa_release/screen/notification_screen/components/tabs/archived_tabs.dart';
import 'package:bisa_release/screen/notification_screen/components/tabs/chat_tabs.dart';
import 'package:bisa_release/screen/notification_screen/components/tabs/unread_tabs.dart';
import 'package:bisa_release/shared/style/styles.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class notif_body extends StatefulWidget {
  const notif_body({Key? key}) : super(key: key);

  @override
  State<notif_body> createState() => _notif_bodyState();
}

class _notif_bodyState extends State<notif_body>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF1CA0E3),
      appBar: AppBar(
        backgroundColor: Color(0xFF1CA0E3),
        leading: IconButton(
          icon: Image.asset(
            "assets/images/icon/back-ico.png",
            height: 30,
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
        elevation: 0,
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: IconButton(
              icon: Icon(
                Icons.info_outline, // Use the info_outline icon
                size: 32,
              ),
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      backgroundColor: Styles.white, // Dark background
                      content: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          // Add your info content here, such as Text or other widgets
                          Text(
                            "This is some info text.",
                            style: Styles.txt14medium
                                .copyWith(color: Styles.primaryColor),
                          ),
                          SizedBox(height: 20),
                          // You can add more widgets as needed
                        ],
                      ),
                      actions: [
                        TextButton(
                          onPressed: () {
                            Navigator.of(context).pop(); // Close the dialog
                          },
                          child: Text(
                            "Close",
                            style: TextStyle(
                              color: Styles.primaryColor,
                              fontSize: 16,
                            ),
                          ),
                        ),
                      ],
                    );
                  },
                );
              },
            ),
          ),
        ],
        title: Text(
          "Notifikasi",
          style: GoogleFonts.poppins(
            color: Color.fromARGB(255, 255, 255, 255),
            fontSize: 22,
            fontWeight: FontWeight.w600,
          ),
        ),
        centerTitle: true,
      ),
      body: Container(
        decoration: const BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Color.fromRGBO(0, 0, 0, 0.22),
              offset: Offset(0, 3),
              blurRadius: 4,
            )
          ],
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(25.0),
            topRight: Radius.circular(25.0),
          ),
        ),
        height: MediaQuery.of(context).size.height - 100,
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TabBar(
                controller: _tabController,
                indicator: UnderlineTabIndicator(
                  borderSide: BorderSide(
                    width: 2.0,
                    color: Styles.primaryColor,
                  ),
                  insets: EdgeInsets.symmetric(horizontal: 10),
                ),
                tabs: [
                  Tab(
                    child: Text(
                      'Unread',
                      style: GoogleFonts.poppins(
                        fontWeight: _tabController.index == 0
                            ? FontWeight.bold
                            : FontWeight.normal,
                        fontSize: _tabController.index == 0 ? 18.0 : 16.0,
                        color: _tabController.index == 0
                            ? Colors.black
                            : Colors.grey,
                      ),
                    ),
                  ),
                  Tab(
                    child: Text(
                      'Chat',
                      style: GoogleFonts.poppins(
                        fontWeight: _tabController.index == 1
                            ? FontWeight.bold
                            : FontWeight.normal,
                        fontSize: _tabController.index == 1 ? 18.0 : 16.0,
                        color: _tabController.index == 1
                            ? Colors.black
                            : Colors.grey,
                      ),
                    ),
                  ),
                  Tab(
                    child: Text(
                      'Archived',
                      style: GoogleFonts.poppins(
                        fontWeight: _tabController.index == 2
                            ? FontWeight.bold
                            : FontWeight.normal,
                        fontSize: _tabController.index == 2 ? 16.0 : 18.0,
                        color: _tabController.index == 2
                            ? Colors.black
                            : Colors.grey,
                      ),
                    ),
                  ),
                ],
                onTap: (index) {
                  setState(() {
                    _tabController.index = index;
                  });
                },
              ),
            ),
            Expanded(
              child: TabBarView(
                controller: _tabController,
                children: [unread_tabs(), chat_tabs(), archived_tabs()],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
