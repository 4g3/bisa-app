import 'package:flutter/material.dart';
import 'package:bisa_release/shared/style/styles.dart';
import 'package:gap/gap.dart';

class unread_tabs extends StatefulWidget {
  const unread_tabs({super.key});

  @override
  State<unread_tabs> createState() => _unread_tabsState();
}

class _unread_tabsState extends State<unread_tabs> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 320,
      decoration: const BoxDecoration(
        color: Color.fromARGB(0, 240, 3, 3),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Row(
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                height: 200,
                decoration: const BoxDecoration(
                    color: Color.fromARGB(0, 255, 255, 255),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(8.0),
                      bottomLeft: Radius.circular(8.0),
                      topRight: Radius.circular(8.0),
                      bottomRight: Radius.circular(8.0),
                    )),
                child: Row(
                  children: [
                    Padding(padding: EdgeInsets.all(12)),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                            width: 310,
                            height: 160,
                            decoration: const BoxDecoration(
                                color: Color.fromARGB(255, 255, 255, 255),
                                boxShadow: [
                                  BoxShadow(
                                      color: Color.fromRGBO(0, 0, 0, 0.22),
                                      offset: Offset(0, 2),
                                      blurRadius: 4)
                                ],
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(8.0),
                                  bottomLeft: Radius.circular(8.0),
                                  topRight: Radius.circular(8.0),
                                  bottomRight: Radius.circular(8.0),
                                )),
                            child: Row(
                              children: [
                                SizedBox(
                                  width: 6,
                                ),
                                Container(
                                  height: 140,
                                  width: 6,
                                  decoration: BoxDecoration(
                                      color: Color(0xff5CB65C),
                                      borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(8.0),
                                        bottomLeft: Radius.circular(8.0),
                                        topRight: Radius.circular(8.0),
                                        bottomRight: Radius.circular(8.0),
                                      )),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text("Selamat Datang 👋",
                                          style: Styles.txt16semibold),
                                      Gap(4),
                                      Text(
                                        "Selamat datang di aplikasi \"BISA\"! Nikmati\npengalaman belajar terbaik dan jangkau\npotensi Anda bersama kami.\nBergabunglah sekarang",
                                        style: Styles.txt12regular,
                                      ),
                                      Spacer(),
                                      Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Icon(
                                            Icons.person,
                                            size: 20,
                                          ),
                                          Gap(2),
                                          Text("Admin",
                                              style: Styles.txt14semibold
                                                  .copyWith(
                                                      color: Color.fromARGB(
                                                          255, 176, 176, 176))),
                                          SizedBox(
                                            width: 170,
                                          ),
                                          Text("06.59",
                                              style: Styles.txt14semibold
                                                  .copyWith(
                                                      color: Color.fromARGB(
                                                          255, 176, 176, 176))),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ))
                      ],
                    )
                  ],
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
