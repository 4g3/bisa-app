import 'package:bisa_release/screen/notification_screen/components/notif_body.dart';
import 'package:flutter/material.dart';

class notification_screen extends StatefulWidget {
  const notification_screen({super.key});

  @override
  State<notification_screen> createState() => _notification_screenState();
}

class _notification_screenState extends State<notification_screen> {
  @override
  Widget build(BuildContext context) {
    return const Scaffold(body: notif_body());
  }
}
