import 'package:bisa_release/screen/login_screen/components/continue_section.dart';
import 'package:bisa_release/screen/login_screen/components/form_section.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';

import 'components/head_section.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
        body: Container(
          child: Column(
            children: [
              Expanded(
                flex: 1,
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      HeadLoginScreen(),
                      Gap(MediaQuery.of(context).size.height * 0.12),
                      FormLoginScreen(),
                      Gap(MediaQuery.of(context).size.height * 0.10),
                      ContinueLoginScreen(),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
