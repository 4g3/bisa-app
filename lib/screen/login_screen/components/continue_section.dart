import 'package:bisa_release/shared/style/styles.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';

class ContinueLoginScreen extends StatelessWidget {
  const ContinueLoginScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.9,
      // decoration: BoxDecoration(
      //     border: Border.all(
      //       color: Styles.black, // Red border color
      //       width: 0.6, // Border width
      //     ),
      //     borderRadius: BorderRadius.circular(8)),
      child: Column(
        children: [
          Gap(16),
          Gap(24),
          Row(
            children: [
              Expanded(child: Container(height: 1, color: Styles.black)),
              Gap(16),
              Text(
                "daftar dengan cara lain",
                style: Styles.txt14medium,
              ),
              Gap(16),
              Expanded(child: Container(height: 1, color: Styles.black)),
            ],
          ),
          Gap(16),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                alignment: Alignment.center,
                padding: EdgeInsets.only(left: 16),
                width: 120,
                height: 50,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    border: Border.all(
                      color: Styles.black,
                      width: 0.6,
                    )),
                child: Row(
                  children: [
                    Image.asset(
                      "assets/images/icon/go-ico.png",
                      width: 20,
                    ),
                    Gap(8),
                    Text(
                      "Google",
                      style: Styles.txt14regular,
                    )
                  ],
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
