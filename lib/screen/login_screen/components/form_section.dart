import 'package:bisa_release/screen/register_screen/register_screen.dart';
import 'package:bisa_release/shared/widget/bottom_nav.dart';
import 'package:bisa_release/shared/widget/share.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:bisa_release/model/userModel.dart';
import '../../../shared/style/styles.dart';

// ignore: must_be_immutable
class FormLoginScreen extends StatefulWidget {
  static String routeName = "/FormLoginScreen";

  @override
  _FormLoginScreenState createState() => _FormLoginScreenState();
}

class _FormLoginScreenState extends State<FormLoginScreen> {
  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  void dispose() {
    // Dispose of the controllers when the widget is disposed
    usernameController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  postData() async {
    print("postData called"); //debug
    if (usernameController.text.isNotEmpty &&
        passwordController.text.isNotEmpty) {
      var response = await http
          .post(Uri.parse("https://bisatrackapp.com/api/login"), body: {
        "username": "${usernameController.text}",
        "password": "${passwordController.text}"
      });
      print("Status Code : ${response.statusCode}");
      print(response.body);

      if (response.statusCode == 200) {
        UserModel user = userModelFromJson(response.body);
        // print("token : ${user.data.token}");
        print("username : ${user.data.username}");
        SharedPreferences pref = await SharedPreferences.getInstance();
        // await pref.setString('token', user.toString());
        await pref.setString('username', user.data.username);
        await pref.setString('email', user.data.email);

        //pindah page
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => BottomNavigationBisaApp()),
            (route) => false);
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: Text("Password atau Username salah")));
      }
    } else {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content:
              Text("Mohon isi terlebih dahulu kolom Username dan password")));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        children: [
          shareWidget().TextFieldLoginRegister(
              usernameController, "Username", "Username", false),
          Gap(4),
          PasswordTextField(
            controller: passwordController,
            hintText: "Password",
            labelText: "Password",
          ),
          Gap(20),
          GestureDetector(
            onTap: postData,
            child: Container(
              alignment: Alignment.center,
              width: 200,
              height: 50,
              decoration: BoxDecoration(
                color: Styles.primaryColor,
                borderRadius: BorderRadius.circular(100),
              ),
              child: Text(
                "Login",
                style: Styles.txt16medium
                    .copyWith(color: Styles.white, fontSize: 18),
              ),
            ),
          ),
          Gap(16),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Belum punya akun ? yuk ",
                style: Styles.txt14medium.copyWith(color: Styles.primaryColor),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => RegisterScreen()),
                  );
                },
                child: Text(
                  "Daftar!",
                  style: Styles.txt14semibold.copyWith(
                    color: Styles.primaryColor,
                    decoration: TextDecoration.underline,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class PasswordTextField extends StatefulWidget {
  final TextEditingController controller;
  final String hintText;
  final String labelText;

  PasswordTextField({
    required this.controller,
    required this.hintText,
    required this.labelText,
  });

  @override
  _PasswordTextFieldState createState() => _PasswordTextFieldState();
}

class _PasswordTextFieldState extends State<PasswordTextField> {
  bool _obscureText = true;

  void _toggleObscureText() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: widget.controller,
      obscureText: _obscureText,
      decoration: InputDecoration(
        hintText: widget.hintText,
        labelText: widget.labelText,
        labelStyle: Styles.txt20medium.copyWith(color: Styles.primaryColor),
        hintStyle:
            Styles.txt16medium.copyWith(fontSize: 18, color: Styles.grey),
        suffixIcon: GestureDetector(
          onTap: _toggleObscureText,
          child: Icon(
            _obscureText ? Icons.visibility : Icons.visibility_off,
            color: Styles.primaryColor,
          ),
        ),
      ),
      style: Styles.txt16medium.copyWith(fontSize: 18),
    );
  }
}
