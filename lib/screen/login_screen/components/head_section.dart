import 'package:bisa_release/shared/style/styles.dart';
import 'package:flutter/material.dart';

class HeadLoginScreen extends StatefulWidget {
  HeadLoginScreen({super.key});

  @override
  State<HeadLoginScreen> createState() => _HeadLoginScreenState();
}

class _HeadLoginScreenState extends State<HeadLoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: MediaQuery.of(context).size.height * 0.03,
        ),
        Text("BISA", style: Styles.txt24bold),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              alignment: Alignment.bottomLeft,
              height: MediaQuery.of(context).size.height * 0.20,
              width: MediaQuery.of(context).size.width * 0.46,
              child: Text(
                "Udah pernah\njalan sama\nkita?",
                style: Styles.txt24semibold.copyWith(fontSize: 24),
              ),
            ),
            Image.asset("assets/images/login_vector.png",
                width: MediaQuery.of(context).size.width * 0.42)
          ],
        )
      ],
    );
  }
}
