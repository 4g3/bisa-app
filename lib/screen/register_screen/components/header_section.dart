import 'package:bisa_release/shared/style/styles.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';

class HeadRegisterScreen extends StatelessWidget {
  HeadRegisterScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          "BISA",
          style: Styles.txt24bold.copyWith(color: Styles.white),
        ),
        Gap(45),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Image.asset("assets/images/register_vector.png",
                width: MediaQuery.of(context).size.width * 0.36),
            Padding(
              padding: const EdgeInsets.only(right: 8),
              child: Container(
                // decoration: BoxDecoration(
                //   border: Border.all(color: Colors.red), // Add a red border
                // ),
                width: MediaQuery.of(context).size.width * 0.50,
                height: MediaQuery.of(context).size.height * 0.16,
                child: Padding(
                  padding: const EdgeInsets.only(top: 18),
                  child: Text(
                    "Langkah hebat\npertamamu\ndengan kami!",
                    textAlign: TextAlign.right,
                    style: Styles.txt24semibold
                        .copyWith(color: Styles.white, fontSize: 23),
                  ),
                ),
              ),
            )
          ],
        ),
        Gap(25),
      ],
    );
  }
}
