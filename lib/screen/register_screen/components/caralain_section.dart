import 'package:bisa_release/shared/style/styles.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';

class CaraLainRegisterScreen extends StatelessWidget {
  const CaraLainRegisterScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Expanded(child: Container(height: 1, color: Styles.white)),
            Gap(16),
            Text(
              "daftar dengan cara lain",
              style: Styles.txt14medium.copyWith(color: Styles.white),
            ),
            Gap(16),
            Expanded(child: Container(height: 1, color: Styles.white)),
          ],
        ),
        Gap(16),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              alignment: Alignment.center,
              padding: EdgeInsets.only(left: 16),
              width: 120,
              height: 50,
              decoration: BoxDecoration(
                  color: Styles.white, borderRadius: BorderRadius.circular(8)),
              child: Row(
                children: [
                  Image.asset(
                    "assets/images/icon/go-ico.png",
                    width: 20,
                  ),
                  Gap(8),
                  Text(
                    "Google",
                    style: Styles.txt14regular,
                  )
                ],
              ),
            ),
          ],
        )
      ],
    );
  }
}
