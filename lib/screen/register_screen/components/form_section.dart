import 'package:bisa_release/screen/login_screen/login_screen.dart';
import 'package:bisa_release/shared/widget/bottom_nav.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gap/gap.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import '../../../shared/style/styles.dart';
import '../../../shared/widget/share.dart';
import '../../../model/userModel.dart';

class FormRegisterScreen extends StatefulWidget {
  static String routeName = "/FormRegisterScreen";
  FormRegisterScreen({Key? key}) : super(key: key);

  @override
  _FormRegisterScreenState createState() => _FormRegisterScreenState();
}

class _FormRegisterScreenState extends State<FormRegisterScreen> {
  late TextEditingController usernameController;
  late TextEditingController passwordController;
  late TextEditingController emailController;
  late TextEditingController numberController;

  postData() async {
    if (usernameController.text.isNotEmpty &&
        emailController.text.isNotEmpty &&
        passwordController.text.isNotEmpty &&
        numberController.text.isNotEmpty) {
      var response = await http
          .post(Uri.parse("https://bisatrackapp.com/api/register"), body: {
        "username": "${usernameController.text}",
        "email": "${emailController.text}",
        "password": "${passwordController.text}",
        "phone_number": "${numberController.text}"
      });
      print("Status Code : ${response.statusCode}");
      print(response.body);

      if (response.statusCode == 200) {
        UserModel user = userModelFromJson(response.body);
        // print("token : ${user.data.token}");
        print("username : ${user.data.username}");
        print("email : ${user.data.email}");
        print("Phone number: ${numberController.text}");
        SharedPreferences pref = await SharedPreferences.getInstance();
        // pref.setString('token', user.toString());
        pref.setString('username', user.data.username);
        pref.setString('email', user.data.email);
        pref.setString('phoneNumber', numberController.text);

        //pindah page
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => BottomNavigationBisaApp()),
            (route) => false);
      } else {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("Error"),
              content: Text(
                  "Maaf, salah satu dari data yang telah anda isi sudah terdaftar."),
              actions: [
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text("OK"),
                ),
              ],
            );
          },
        );
      }
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text("Isi Semua Kolom dengan benar")));
      Future.delayed(Duration(seconds: 5), () {
        ScaffoldMessenger.of(context).hideCurrentSnackBar();
      });
    }
  }

  @override
  void initState() {
    super.initState();
    usernameController = TextEditingController();
    passwordController = TextEditingController();
    emailController = TextEditingController();
    numberController = TextEditingController();
    numberController.addListener(_updatePhoneNumber);
  }

  @override
  void dispose() {
    usernameController.dispose();
    passwordController.dispose();
    emailController.dispose();
    numberController.removeListener(_updatePhoneNumber);
    numberController.dispose();
    super.dispose();
  }

  void _updatePhoneNumber() {
    String phoneNumber = numberController.text;

    if (phoneNumber.startsWith('0')) {
      phoneNumber = '+62' + phoneNumber.substring(1);
    }

    numberController.value = numberController.value.copyWith(
      text: phoneNumber,
      selection: TextSelection.collapsed(offset: phoneNumber.length),
    );
  }

  void _showValidationPopup(String fields) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Mohon Periksa Kembali !'),
          content: Text('Tolong isi kolom berikut dengan benar:\n$fields'),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text('OK'),
            ),
          ],
        );
      },
    );
  }

  void _handleDaftarButtonTap() {
    if (usernameController.text.isEmpty ||
        passwordController.text.isEmpty ||
        emailController.text.isEmpty ||
        numberController.text.isEmpty) {
      List<String> emptyFields = [];
      if (usernameController.text.isEmpty) emptyFields.add('Username');
      if (passwordController.text.isEmpty) emptyFields.add('Password');
      if (emailController.text.isEmpty) emptyFields.add('Email');
      if (numberController.text.isEmpty) emptyFields.add('Nomor Telepon');

      _showValidationPopup(emptyFields.join(', '));
    } else {
      String phoneNumber = numberController.text;

      if (phoneNumber.length < 8 ||
          int.tryParse(phoneNumber.substring(1)) == null) {
        _showValidationPopup(
            'Nomor Telepon harus memiliki minimal 8 digit angka.');
        return;
      }

      String password = passwordController.text;

      // Check if the password meets the criteria
      bool hasCapitalLetter = password.contains(RegExp(r'[A-Z]'));
      bool hasNumber = password.contains(RegExp(r'[0-9]'));

      if (!hasCapitalLetter || !hasNumber || password.length < 8) {
        _showValidationPopup(
            'Password harus memiliki 8 digit, termasuk huruf capital dan angka.');
        return;
      }

      // String email = emailController.text;

      // Add your own email validation logic here if needed

      postData();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 18, vertical: 16),
      width: double.infinity,
      decoration: BoxDecoration(
        color: Styles.white,
        borderRadius: BorderRadius.circular(16),
      ),
      child: Column(
        children: [
          shareWidget().TextFieldLoginRegister(
              usernameController, "Username", "Username", false),
          Gap(4),
          shareWidget()
              .TextFieldLoginRegister(emailController, "Email", "Email", false),
          Gap(4),
          PasswordTextField(
            controller: passwordController,
            hintText: "Password",
            labelText: "Password",
          ),
          Gap(4),
          TextFormField(
            controller: numberController,
            decoration: InputDecoration(
              hintText: "Nomor Telepon",
              labelText: "Nomor Telepon",
              labelStyle:
                  Styles.txt20medium.copyWith(color: Styles.primaryColor),
              hintStyle: Styles.txt14medium.copyWith(color: Styles.grey),
            ),
            keyboardType: TextInputType.phone,
            inputFormatters: [
              FilteringTextInputFormatter.digitsOnly,
            ],
            style: Styles.txt14medium.copyWith(color: Styles.black),
          ),
          Gap(20),
          GestureDetector(
            onTap: _handleDaftarButtonTap,
            child: Container(
              alignment: Alignment.center,
              width: 200,
              height: 50,
              decoration: BoxDecoration(
                color: Styles.primaryColor,
                borderRadius: BorderRadius.circular(100),
              ),
              child: Text(
                "Daftar",
                style: Styles.txt16medium
                    .copyWith(color: Styles.white, fontSize: 18),
              ),
            ),
          ),
          Gap(10),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Sudah punya akun ? yuk ",
                style: Styles.txt14medium.copyWith(color: Styles.primaryColor),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => LoginScreen()));
                },
                child: Text(
                  "Login",
                  style: Styles.txt14semibold.copyWith(
                      color: Styles.primaryColor,
                      decoration: TextDecoration.underline),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class PasswordTextField extends StatefulWidget {
  final TextEditingController controller;
  final String hintText;
  final String labelText;

  PasswordTextField({
    required this.controller,
    required this.hintText,
    required this.labelText,
  });

  @override
  _PasswordTextFieldState createState() => _PasswordTextFieldState();
}

class _PasswordTextFieldState extends State<PasswordTextField> {
  bool _obscureText = true;

  void _toggleObscureText() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: widget.controller,
      obscureText: _obscureText,
      decoration: InputDecoration(
        hintText: widget.hintText,
        labelText: widget.labelText,
        labelStyle: Styles.txt20medium.copyWith(color: Styles.primaryColor),
        hintStyle:
            Styles.txt16medium.copyWith(fontSize: 18, color: Styles.grey),
        suffixIcon: GestureDetector(
          onTap: _toggleObscureText,
          child: Icon(
            _obscureText ? Icons.visibility : Icons.visibility_off,
            color: Styles.primaryColor,
          ),
        ),
      ),
      style: Styles.txt16medium.copyWith(fontSize: 18),
    );
  }
}
