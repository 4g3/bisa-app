import 'package:bisa_release/screen/register_screen/components/caralain_section.dart';
import 'package:bisa_release/screen/register_screen/components/form_section.dart';
import 'package:bisa_release/screen/register_screen/components/header_section.dart';
import 'package:bisa_release/shared/style/styles.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({super.key});

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        backgroundColor: Styles.primaryColor,
        resizeToAvoidBottomInset: false,
        body: SafeArea(
          child: SingleChildScrollView(
            // Wrap the content with SingleChildScrollView
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  child: Column(
                    children: [
                      HeadRegisterScreen(),
                      FormRegisterScreen(),
                      Gap(24),
                      CaraLainRegisterScreen(),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
