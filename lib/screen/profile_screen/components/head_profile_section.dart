// ignore_for_file: unused_local_variable

import 'package:bisa_release/shared/style/styles.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HeadProfileScreen extends StatefulWidget {
  HeadProfileScreen({super.key});

  @override
  State<HeadProfileScreen> createState() => _HeadProfileScreenState();
}

class _HeadProfileScreenState extends State<HeadProfileScreen> {
  String? username;
  String? phoneNumber;
  String? email;

  @override
  void initState() {
    super.initState();
    getUsername();
  }

  Future<void> getUsername() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setString('phoneNumber', phoneNumber ?? "+62 82171936576");
    String? savedUsername = pref.getString('username');
    String? savedPhoneNumber = pref.getString('phoneNumber');
    String? savedEmail = pref.getString('email');

    setState(() {
      username = savedUsername;
      phoneNumber = savedPhoneNumber;
      email = savedEmail;
    });
  }

  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 230,
      decoration: BoxDecoration(color: Styles.primaryColor),
      child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
        Stack(
          children: [
            CircleAvatar(
              radius: 50,
              backgroundImage: AssetImage("assets/images/icon/acc.png"),
            ),
            Positioned(
              bottom: 0,
              right: 0,
              child: GestureDetector(
                onTap: () {
                  // Handle edit icon tap
                },
                child: Container(
                  width: 32,
                  height: 32,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.circle,
                    border: Border.all(color: Styles.primaryColor, width: 2.5
                        // Outer border color
                        ),
                  ),
                  child: Icon(
                    Icons.edit_rounded,
                    size: 18,
                    color: Styles.primaryColor, // Icon color
                  ),
                ),
              ),
            ),
          ],
        ),
        Text(
          username ?? "Default Username",
          style: Styles.txt24semibold.copyWith(color: Styles.white),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              phoneNumber ?? "-",
              style: Styles.txt12regular.copyWith(color: Styles.white),
            ),
          ],
        ),
        Text(
          email ?? "andra@gmail.com",
          style: Styles.txt16semibold.copyWith(color: Styles.white),
        ),
      ]),
    );
  }
}
