// ignore_for_file: unused_local_variable

import 'package:bisa_release/shared/style/styles.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:shared_preferences/shared_preferences.dart';

class InfoProfileScreen extends StatefulWidget {
  const InfoProfileScreen({super.key});

  @override
  State<InfoProfileScreen> createState() => _InfoProfileScreenState();
}

class _InfoProfileScreenState extends State<InfoProfileScreen> {
  String? username;
  String? phoneNumber;
  String? email;
  @override
  void initState() {
    super.initState();
    getUsername();
  }

  Future<void> getUsername() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String? savedUsername = pref.getString('username');
    String? savedPhoneNumber = pref.getString('phoneNumber');
    String? savedEmail = pref.getString('email');

    setState(() {
      username = username;
      phoneNumber = savedPhoneNumber;
      email = savedEmail;
    });
  }

  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16).copyWith(top: 200),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(10),
            width: double.infinity,
            height: 100,
            decoration: BoxDecoration(boxShadow: [
              BoxShadow(
                  color: Styles.black.withOpacity(0.25),
                  blurRadius: 20,
                  offset: Offset(0, 4))
            ], color: Styles.white, borderRadius: BorderRadius.circular(10)),
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Row(
                children: [
                  Text(
                    "Tentang Saya",
                    style: Styles.txt14semibold,
                  ),
                  Spacer(),
                  GestureDetector(
                    onTap: () {},
                    child: Icon(
                      Icons.edit_rounded,
                      color: Styles.grey,
                      size: 20,
                    ),
                  )
                ],
              ),
              Gap(4),
              Container(
                width: 310,
                child: Text(
                  "Aku adalah murid yang suka programming dan esport. Menikmati membangun program dan bermain game membuatku senang dan terus belajar.",
                  style: Styles.txt10medium.copyWith(color: Styles.grey),
                ),
              )
            ]),
          ),
          Gap(24),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 16, vertical: 20),
            width: double.infinity,
            height: 320,
            decoration: BoxDecoration(boxShadow: [
              BoxShadow(
                  color: Styles.black.withOpacity(0.25),
                  blurRadius: 20,
                  offset: Offset(0, 4))
            ], color: Styles.white, borderRadius: BorderRadius.circular(10)),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Text(
                        "INFORMASI AKUN",
                        style: Styles.txt14semibold,
                      ),
                      Spacer(),
                      GestureDetector(
                        onTap: () {},
                        child: Icon(
                          Icons.edit_rounded,
                          color: Styles.grey,
                          size: 20,
                        ),
                      ),
                    ],
                  ),
                  Gap(1),
                  Divider(
                    color: Styles.grey,
                  ),
                  Row(
                    children: [
                      Text(
                        "Nama Lengkap",
                        style: Styles.txt14medium.copyWith(color: Styles.grey),
                      ),
                      Spacer(),
                      Text(username ?? "-",
                          style:
                              Styles.txt14medium.copyWith(color: Styles.black))
                    ],
                  ),
                  Gap(2),
                  Divider(
                    color: Styles.grey,
                  ),
                  Gap(0),
                  Row(
                    children: [
                      Text(
                        "Nama Sekolah",
                        style: Styles.txt14medium.copyWith(color: Styles.grey),
                      ),
                      Spacer(),
                      Text("-",
                          style:
                              Styles.txt14medium.copyWith(color: Styles.black))
                    ],
                  ),
                  Gap(2),
                  Divider(
                    color: Styles.grey,
                  ),
                  Gap(2),
                  Row(
                    children: [
                      Text(
                        "Kelas / Tingkatan",
                        style: Styles.txt14medium.copyWith(color: Styles.grey),
                      ),
                      Spacer(),
                      Text("-",
                          style:
                              Styles.txt14medium.copyWith(color: Styles.black))
                    ],
                  ),
                  Gap(2),
                  Divider(
                    color: Styles.grey,
                  ),
                  Gap(2),
                  Row(
                    children: [
                      Text(
                        "Alamat",
                        style: Styles.txt14medium.copyWith(color: Styles.grey),
                      ),
                      Spacer(),
                      Text("-",
                          style:
                              Styles.txt14medium.copyWith(color: Styles.black))
                    ],
                  ),
                  Gap(2),
                  Divider(
                    color: Styles.grey,
                  ),
                  Gap(2),
                  Row(
                    children: [
                      Text(
                        "Nomor telepon",
                        style: Styles.txt14medium.copyWith(color: Styles.grey),
                      ),
                      Spacer(),
                      Text(phoneNumber ?? "08123456789",
                          style:
                              Styles.txt14medium.copyWith(color: Styles.black))
                    ],
                  ),
                  Gap(2),
                  Divider(
                    color: Styles.grey,
                  ),
                  Gap(2),
                  Row(
                    children: [
                      Text(
                        "Email",
                        style: Styles.txt14medium.copyWith(color: Styles.grey),
                      ),
                      Spacer(),
                      Text(email ?? "andra@gmail.com",
                          style:
                              Styles.txt14medium.copyWith(color: Styles.black))
                    ],
                  ),
                  Gap(2),
                  Divider(
                    color: Styles.grey,
                  ),
                ]),
          )
        ],
      ),
    );
  }
}
