// import 'package:bisa_release/screen/home_screen/home_screen.dart';
import 'package:bisa_release/screen/login_screen/login_screen.dart';
import 'package:bisa_release/screen/profile_screen/components/head_profile_section.dart';
import 'package:bisa_release/screen/profile_screen/components/info_profile_section.dart';
import 'package:bisa_release/shared/style/styles.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({super.key});

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Scaffold(
            appBar: AppBar(
              backgroundColor: Styles.primaryColor,
              leading: IconButton(
                icon: Image.asset(
                  "assets/images/icon/back-ico.png",
                  height: 30,
                ),
                onPressed: () => Navigator.of(context).pop(),
              ),
              centerTitle: true,
              title: Text(
                "Profil",
                style: Styles.txt24medium.copyWith(color: Colors.white),
              ),
              actions: [
                GestureDetector(
                    onTap: () async {
                      SharedPreferences pref =
                          await SharedPreferences.getInstance();
                      await pref.remove('username');
                      await pref.remove('token');
                      await pref.remove('email');
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: Row(
                              children: [
                                Icon(Icons.logout,
                                    color: Styles.primaryColor), // Logout icon
                                SizedBox(width: 8),
                                Text("Logout"),
                              ],
                            ),
                            content: Text(
                              "Apakah anda yakin ingin logout dari akun ini?",
                              style: Styles.txt14medium,
                            ),
                            actions: [
                              Container(
                                width: 70,
                                height: 40, // Adjust the width as needed
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(
                                      8), // Rounded corners
                                  border: Border.all(
                                    color: Styles.primaryColor, // Border color
                                  ),
                                  color: Colors.white, // White fill
                                ),
                                child: TextButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                  child: Text(
                                    "TIDAK",
                                    style: Styles.txt14medium
                                        .copyWith(color: Styles.primaryColor),
                                  ),
                                ),
                              ),
                              SizedBox(width: 0), // Add spacing between buttons
                              Container(
                                width: 70,
                                height: 40,
                                // Adjust the width as needed
                                decoration: BoxDecoration(
                                  color: Styles.primaryColor,
                                  borderRadius: BorderRadius.circular(
                                      8), // Rounded corners
                                ),
                                child: TextButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => LoginScreen()),
                                    );
                                  },
                                  child: Text(
                                    "YAKIN",
                                    style: Styles.txt14medium
                                        .copyWith(color: Colors.white),
                                  ),
                                ),
                              ),
                            ],
                          );
                        },
                      );
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(right: 16),
                      child: Icon(Icons.logout_rounded),
                    ))
              ],
              elevation: 0,
            ),
            resizeToAvoidBottomInset: true,
            body: SafeArea(
              child: SingleChildScrollView(
                  child: Column(
                children: [
                  Stack(
                    children: [HeadProfileScreen(), InfoProfileScreen()],
                  )
                ],
              )),
            )));
  }
}
