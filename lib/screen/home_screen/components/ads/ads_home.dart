import 'package:gap/gap.dart';
import 'package:flutter/material.dart';

import '../../../../shared/style/styles.dart';

class ads_home extends StatefulWidget {
  const ads_home({super.key});

  @override
  State<ads_home> createState() => _ads_homeState();
}

class _ads_homeState extends State<ads_home> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: Text(
              "Cek patner kami juga ya!",
              style: Styles.txt16medium,
            ),
          ),
          Gap(12),
          Center(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: 80,
                decoration: BoxDecoration(
                  color: Styles.white,
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: Styles.grey, width: 1),
                ),
                child: Column(
                  children: [
                    Gap(16),
                    Text(
                      "AD SPACE",
                      style: TextStyle(
                        color: Colors.grey, // Add grey color here
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Gap(4),
                    Text(
                      "Konsultasi dengan ahli",
                      style: TextStyle(
                        color: Colors.grey, // Add grey color here
                        fontSize: 10,
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
