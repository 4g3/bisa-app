import 'package:bisa_release/screen/quiztest_screen/quiztest_screen.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import '../../../shared/style/styles.dart';
import '../../../shared/widget/share.dart';
import '../../../shared/temp/Quizdata.dart';

class RekomendasiParaAhliHomeScreen extends StatefulWidget {
  const RekomendasiParaAhliHomeScreen({super.key});

  @override
  State<RekomendasiParaAhliHomeScreen> createState() =>
      _RekomendasiParaAhliHomeScreenState();
}

class _RekomendasiParaAhliHomeScreenState
    extends State<RekomendasiParaAhliHomeScreen> {
  final List<bool> _bookmarks =
      List.filled(10, false); // List to track bookmark state

  final dummyData = Quizdata.getListData();

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 16),
      width: double.infinity,
      height: 230,
      decoration: BoxDecoration(color: Styles.primaryColor),
      child: Row(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 150,
                child: Text(
                  "Rekomendasi para ahli untuk kalian.",
                  style: Styles.txt20semibold
                      .copyWith(color: Styles.white, height: 1.2),
                ),
              ),
              Gap(8),
              Image.asset(
                "assets/images/rekom.png",
                width: 150,
              ),
            ],
          ),
          Gap(16),
          Expanded(
            child: GestureDetector(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => quiztest()));
              },
              child: Container(
                height: 160,
                child: ListView.builder(
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    itemCount: 5,
                    itemBuilder: (context, index) {
                      return Stack(
                        children: [
                          Container(
                            margin: EdgeInsets.only(right: 16),
                            width: 140,
                            decoration: BoxDecoration(
                                color: Styles.white,
                                borderRadius: BorderRadius.circular(12)),
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    height: 100,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.only(
                                          bottomLeft: Radius.circular(12),
                                          bottomRight: Radius.circular(12)),
                                      image: DecorationImage(
                                          image: AssetImage(
                                              "${dummyData[index]['image']}"),
                                          fit: BoxFit.cover),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.black.withOpacity(0.25),
                                          spreadRadius: 0,
                                          blurRadius: 8,
                                          offset: Offset(0,
                                              2), // changes position of shadow
                                        ),
                                      ],
                                    ),
                                    child: Container(
                                      padding:
                                          EdgeInsets.only(left: 8, bottom: 8),
                                      width: double.infinity,
                                      alignment: Alignment.bottomLeft,
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.only(
                                              bottomLeft: Radius.circular(12),
                                              bottomRight: Radius.circular(12)),
                                          gradient: LinearGradient(
                                              begin: Alignment.bottomCenter,
                                              end: Alignment.topCenter,
                                              stops: [
                                                0.09,
                                                0.5
                                              ],
                                              colors: [
                                                Styles.black.withOpacity(1),
                                                Styles.black.withOpacity(0.0),
                                              ])),
                                      child: Text(
                                        "${dummyData[index]['title']}",
                                        style: Styles.txt14semibold
                                            .copyWith(color: Styles.white),
                                      ),
                                    ),
                                  ),
                                  Gap(5),
                                  Padding(
                                    padding: EdgeInsets.only(left: 4.0),
                                    child: Text(
                                      " ${dummyData[index]['status']}",
                                      style: Styles.txt12semibold
                                          .copyWith(fontSize: 12),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(left: 6.0),
                                    child: Text(
                                      "${dummyData[index]['soal']}",
                                      style: Styles.txt10medium.copyWith(
                                          fontSize: 9.5, color: Styles.grey),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(left: 3.0),
                                    child: Row(
                                      children: [
                                        shareWidget().FiveStar(16),
                                        Text(
                                          "${dummyData[index]['rating']}",
                                          style: Styles.txt8medium
                                              .copyWith(color: Styles.grey),
                                        )
                                      ],
                                    ),
                                  ),
                                ]),
                          ),
                          Positioned(
                            top: 85,
                            left: 85,
                            child: Container(
                                width: 40,
                                height: 40,
                                decoration: BoxDecoration(
                                    color: Styles.white,
                                    borderRadius: BorderRadius.circular(5),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.black.withOpacity(0.25),
                                        spreadRadius: 0,
                                        blurRadius: 4,
                                        offset: Offset(
                                            0, 4), // changes position of shadow
                                      ),
                                    ]),
                                child: GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      _bookmarks[index] = !_bookmarks[
                                          index]; // Toggle bookmark state
                                    });
                                  },
                                  child: Icon(
                                    _bookmarks[index]
                                        ? Icons.bookmark
                                        : Icons.bookmark_border_rounded,
                                    color: Styles.darkBlue,
                                    size: 32,
                                  ),
                                )),
                          ),
                        ],
                      );
                    }),
              ),
            ),
          )
        ],
      ),
    );
  }
}
