import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:stories_for_flutter/stories_for_flutter.dart';
import '../../../shared/style/styles.dart';
import 'package:http/http.dart' as http;

class HighlightHomeScreen extends StatefulWidget {
  const HighlightHomeScreen({Key? key}) : super(key: key);

  @override
  State<HighlightHomeScreen> createState() => _HighlightHomeScreenState();
}

class _HighlightHomeScreenState extends State<HighlightHomeScreen> {
  List<Map<String, dynamic>> storyList = [];
  bool isLoaded = false;
  String token = "";

  @override
  void initState() {
    super.initState();
    getStoryData();
  }

  Future<void> getStoryData() async {
    final response = await http.get(
      Uri.parse('https://bisatrackapp.com/api/story'),
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );

    if (response.statusCode == 200) {
      final List<dynamic> jsonData = json.decode(response.body)['data'];
      print(response.body);
      setState(() {
        storyList = jsonData.map((e) => e as Map<String, dynamic>).toList();
        isLoaded = true;
      });
    } else {
      print('Failed to load data'); // Handle the error gracefully
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 12, vertical: 10),
      width: double.infinity,
      height: 160,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(12),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.25),
            spreadRadius: 0,
            blurRadius: 8,
            offset: Offset(0, 2), // changes position of shadow
          ),
        ],
      ),
      child: Column(
        children: [
          Row(
            children: [
              Text(
                "Highlight",
                style: Styles.txt14medium,
              ),
              Spacer(),
              Text(
                "BISA",
                style: Styles.txt24bold,
              ),
            ],
          ),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: [
                if (!isLoaded)
                  CircularProgressIndicator()
                else if (storyList.isNotEmpty)
                  Stories(
                    circlePadding: 2,
                    storyItemList: [
                      for (var data in storyList)
                      StoryItem(
                          name: (data['name'] as String? ?? "").substring(
                            0,
                          ), // Limit to 12 characters for name
                          thumbnail:
                              NetworkImage(data['logo'] as String? ?? ""),
                          stories: [
                            Scaffold(
                              body: Stack(
                                children: [
                                  Container(
                                    decoration: BoxDecoration(
                                      image: DecorationImage(
                                        fit: BoxFit.cover,
                                        image: NetworkImage(
                                            data['image'] as String? ?? ""),
                                      ),
                                    ),
                                  ),
                                  Positioned(
                                    bottom:
                                        16, // Adjust as needed for caption placement
                                    left:
                                        16, // Adjust as needed for caption placement
                                    right:
                                        16, // Adjust as needed for caption placement
                                    child: Container(
                                      color: Colors.black.withOpacity(
                                          0.5), // Adjust opacity as needed
                                      padding: EdgeInsets.all(8),
                                      child: Text(
                                        data['description'] as String? ?? "",
                                        style: Styles.txt16medium.copyWith(
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Scaffold(
                              body: Container(
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                    fit: BoxFit.cover,
                                    image: NetworkImage(
                                        data['images'] as String? ?? ""),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      // Add more StoryItem widgets as needed
                    ],
                  )
                else
                  Text('No stories available.'),
              ],
            ),
          )
        ],
      ),
    );
  }
}
