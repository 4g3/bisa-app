import 'package:bisa_release/screen/artikel_screen/artikel_screen.dart';
import 'package:bisa_release/shared/style/styles.dart';
import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;
import 'dart:convert';

class ArtikelPilihanHomeScreen extends StatefulWidget {
  const ArtikelPilihanHomeScreen({Key? key}) : super(key: key);

  @override
  State<ArtikelPilihanHomeScreen> createState() =>
      _ArtikelPilihanHomeScreenState();
}

class _ArtikelPilihanHomeScreenState extends State<ArtikelPilihanHomeScreen> {
  List<Map<String, dynamic>> articleList = [];
  bool isLoading = true;

  Future<void> fetchArticles() async {
    final response = await http
        .get(Uri.parse('https://bisatrackapp.com/api/posts'), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ${"L063oh5qw3uHqxHwXc6KjGYF35tB1aBw485p9HnE"}',
    });

    if (response.statusCode == 200) {
      final List<dynamic> jsonData = json.decode(response.body)['data'];
      setState(() {
        articleList = jsonData.map((e) => e as Map<String, dynamic>).toList();
      });
    } else {
      // throw Exception('Failed to load data');
    }
  }

  @override
  void initState() {
    super.initState();
    try {
      fetchArticles().then((_) {
        setState(() {
          isLoading = false;
        });
      });
    } catch (error) {
      print('Error in initState: $error');
      setState(() {
        isLoading = false; // Juga nonaktifkan loading jika terjadi kesalahan
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        alignment: Alignment.centerLeft,
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Artikel pilihan",
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 12),
            isLoading
                ? CircularProgressIndicator() // Display loading indicator while fetching data
                : articleList.isEmpty
                    ? Text(
                        '- Maaf , tidak ada artikel tersedia untuk saat ini -',
                        style: Styles.txt12medium,
                      ) // Display a message if no articles are available
                    : Row(
                        children: [
                          Expanded(
                            child: GestureDetector(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => artikel_screen(),
                                  ),
                                );
                              },
                              child: Container(
                                height: 160,
                                child: ListView.builder(
                                  shrinkWrap: true,
                                  scrollDirection: Axis.horizontal,
                                  itemCount: articleList.length,
                                  itemBuilder: (context, index) {
                                    return ClipRRect(
                                      borderRadius: BorderRadius.circular(12),
                                      child: Container(
                                        margin: EdgeInsets.only(right: 16),
                                        width: 140,
                                        decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(12),
                                          boxShadow: [
                                            BoxShadow(
                                              color: Colors.black
                                                  .withOpacity(0.25),
                                              spreadRadius: 0,
                                              blurRadius: 4,
                                              offset: Offset(0, 4),
                                            ),
                                          ],
                                        ),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Container(
                                              height: 90,
                                              decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(12),
                                                image: DecorationImage(
                                                  image: NetworkImage(
                                                      '${articleList[index]['image']}'),
                                                  fit: BoxFit.cover,
                                                ),
                                              ),
                                            ),
                                            SizedBox(height: 12),
                                            Padding(
                                              padding:
                                                  EdgeInsets.only(left: 12.0),
                                              child: Text(
                                                "${articleList[index]['title']}",
                                                style: TextStyle(
                                                  fontSize: 10,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ),
                                            Padding(
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: 12),
                                              child: Container(
                                                width: 140,
                                                child: Text(
                                                  "${articleList[index]['content']}",
                                                  maxLines: 2,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  style: TextStyle(
                                                    color: Colors.grey,
                                                    fontSize: 8,
                                                  ),
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    );
                                  },
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
          ],
        ),
      ),
    );
  }
}
