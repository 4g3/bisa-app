import 'package:flutter/material.dart';

import '../../../shared/style/styles.dart';

class PekerjaanHomeScreen extends StatelessWidget {
  const PekerjaanHomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          alignment: Alignment.centerLeft,
            child: Text("Pekerjaan paling banyak dicari", style: Styles.txt16medium,)),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Image.asset(
                'assets/images/icon/dm-ico.png',
                width: 90,
              ),
              Image.asset(
                'assets/images/icon/content-ico.png',
                width: 90,
              ),
              Image.asset(
                'assets/images/icon/videoedit-ico.png',
                width: 90,
              ),
              Image.asset(
                'assets/images/icon/etc-ico.png',
                width: 90,
              ),
            ],
          ),
        )
      ],
    );
  }
}
