import 'package:bisa_release/screen/explore_screen/explore_screen.dart';
import 'package:bisa_release/screen/notification_screen/notification_screen.dart';
import 'package:bisa_release/screen/profile_screen/profile_screen.dart';
import 'package:bisa_release/shared/style/styles.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:speech_to_text/speech_to_text.dart'
    as stt; // Import the speech_to_text package

class SliverAppbarHomeScreen extends StatefulWidget {
  const SliverAppbarHomeScreen({super.key});

  @override
  State<SliverAppbarHomeScreen> createState() => _SliverAppbarHomeScreenState();
}

class _SliverAppbarHomeScreenState extends State<SliverAppbarHomeScreen> {
  bool _isChampion = false;
  String? username;
  String? phoneNumber;
  String? email;
  final TextEditingController _searchController = TextEditingController();
  final stt.SpeechToText _speech =
      stt.SpeechToText(); // Create an instance of SpeechToText

  @override
  void initState() {
    super.initState();
    getUsername();
  }

  Future<void> getUsername() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String? savedUsername = pref.getString('username');
    String? savedPhoneNumber = pref.getString('phoneNumber');
    String? savedEmail = pref.getString('email');

    setState(() {
      username = savedUsername;
      phoneNumber = savedPhoneNumber;
      email = savedEmail;
    });
  }

  void startListening() async {
    if (await _speech.initialize()) {
      _speech.listen(
        onResult: (result) {
          setState(() {
            _searchController.text =
                result.recognizedWords; // Update the search bar text
          });
        },
      );
    } else {
      print('Speech recognition not available or permission denied');
    }
  }

  void stopListening() {
    _speech.stop();
  }

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      automaticallyImplyLeading: false,
      backgroundColor: Styles.primaryColor,
      collapsedHeight: 60,
      expandedHeight: 240,
      floating: false,
      pinned: true,
      flexibleSpace: FlexibleSpaceBar(
        titlePadding: EdgeInsets.only(bottom: 2),
        centerTitle: true,
        expandedTitleScale: 1.05,
        title: Container(
          margin: EdgeInsets.symmetric(horizontal: 10, vertical: 4),
          width: double.infinity,
          height: 50,
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.25),
              spreadRadius: 0,
              blurRadius: 8,
              offset: Offset(0, 1),
            ),
          ]),
          child: TextField(
            controller: _searchController,
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ExploreScreen()),
              );
            },
            decoration: InputDecoration(
              hintText: "Cari sekolah dan artikel terkait",
              suffixIcon: GestureDetector(
                onTap: () {
                  // Start voice recognition when the microphone icon is tapped
                  startListening();
                },
                child: Icon(
                  Icons.mic_rounded,
                  color: Styles.grey,
                ),
              ),
              hintStyle: Styles.txt14regular
                  .copyWith(color: Color(0xFFC1C1C1), height: 1),
              prefixIcon: Icon(
                Icons.search_rounded,
                color: Styles.grey,
              ),
              filled: true,
              fillColor: Colors.white,
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(100),
                borderSide: BorderSide.none,
              ),
            ),
          ),
        ),
        background: Container(
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              //Parent Row Header
              Row(
                children: [
                  //Row untuk avatar dan info user
                  Row(
                    children: [
                      //avatar
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => ProfileScreen(),
                            ),
                          );
                        },
                        child: CircleAvatar(
                          radius: 25,
                          backgroundImage:
                              AssetImage('assets/images/icon/acc.png'),
                        ),
                      ),
                      Gap(8),
                      // Column untuk info user
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          //Row untuk nama dan icon plus
                          Row(
                            children: [
                              Text('$username',
                                  style: Styles.txt14bold
                                      .copyWith(color: Styles.white)),
                              Gap(2),
                              Icon(
                                Icons.add_circle_outline_rounded,
                                color: Styles.white,
                              )
                            ],
                          ),
                          //Row untuk status pendidikan user
                          Row(
                            children: [
                              Text(
                                '$email',
                                style: Styles.txt12regular
                                    .copyWith(color: Styles.white),
                              ),
                            ],
                          )
                        ],
                      )
                    ],
                  ),
                  Spacer(),
                  //Container Calon Pemimpin
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        _isChampion = !_isChampion;
                      });
                    },
                    child: AnimatedSwitcher(
                      duration: Duration(milliseconds: 300),
                      switchInCurve: Curves.easeOutCirc,
                      switchOutCurve: Curves.easeOutCirc,
                      child: Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: 8, vertical: 4),
                        width: 60,
                        height: 25,
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [
                              Color(0xFFBFA254),
                              Color(0xFFE3D09C),
                            ],
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                          ),
                          borderRadius: BorderRadius.circular(100),
                        ),
                        child: Center(
                          child: Text(
                            "Gold",
                            style: Styles.txt12semibold
                                .copyWith(color: Styles.white),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Gap(6),
                  //Icon Lonceng
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => notification_screen(),
                        ),
                      );
                    },
                    child: Icon(
                      Icons.notifications_none_rounded,
                      size: 33,
                      color: Styles.white,
                    ),
                  )
                ],
              ),
              Gap(32),
              //Container kata kata
              Stack(children: [
                Positioned(
                  left: 155,
                  top: 16,
                  child: Container(
                      // decoration: BoxDecoration(
                      //   border: Border.all(color: Colors.red, width: 2),
                      // ),
                      width: 200,
                      child: Image.asset('assets/images/h1-home.png')),
                ),
                Positioned(
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height * 0.15,
                    child: Text(
                      "Cari tau Diri lo \nyang sesungguhnya.",
                      style: Styles.txt16semibold
                          .copyWith(color: Styles.white, fontSize: 20),
                    ),
                  ),
                ),
              ]),
            ],
          ),
        ),
      ),
    );
  }
}
