import 'package:bisa_release/shared/style/styles.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';

class CeritaKomunitasHomeScreen extends StatefulWidget {
  const CeritaKomunitasHomeScreen({super.key});

  @override
  State<CeritaKomunitasHomeScreen> createState() => _CeritaKomunitasHomeScreenState();
}

class _CeritaKomunitasHomeScreenState extends State<CeritaKomunitasHomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Sedikit cerita dari komunitas", style: Styles.txt16medium,),
          Gap(10),
          Row(
            children: [
              Expanded(
                  child: Container(
                height: 140,
                child: ListView.builder(
                  itemCount: 5,
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) {
                  return Container(
                    padding: EdgeInsets.symmetric(horizontal:16, vertical: 8),
                    margin: EdgeInsets.only(right: 16),
                    width: 280,
                    decoration: BoxDecoration(
                      color: Styles.white,
                      borderRadius: BorderRadius.circular(12),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black.withOpacity(0.25),
                          spreadRadius: 0,
                          blurRadius: 12,
                          offset: Offset(0, 4), // changes position of shadow
                        ),
                      ],
                    ),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            CircleAvatar(
                              child: Image.asset("assets/images/avDis.png"),
                            ),
                            Gap(8),
                            Column(
                              children: [
                                Text("Rangga A.", style: Styles.txt14semibold,),
                                Text("1 hari yang lalu", style: Styles.txt10regular)
                              ],
                            )
                          ],
                        ),
                        Gap(12),
                        Container(
                          width: 240,
                            child: Text("gais menurut kalian metode belajar ngebut semalam untuk ujian 3 mapel efektif gak ? 😀", style: Styles.txt10medium,)),
                        Gap(24),
                        Row(
                          children: [
                            Icon(Icons.thumb_up_alt_outlined, size: 16,),
                            Gap(4),
                            Text("12", style: Styles.txt10medium,),
                            Gap(16),
                            Icon(Icons.insert_comment_rounded, size: 16,),
                            Gap(4),
                            Text("12", style: Styles.txt10medium,),

                          ],
                        )
                      ],
                    ),
                  );
                }),
              )),
            ],
          )
        ],
      ),
    );
  }
}
