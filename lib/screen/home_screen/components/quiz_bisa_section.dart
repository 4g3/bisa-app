import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:bisa_release/shared/temp/Quizdata.dart';
import '../../../shared/style/styles.dart';
import '../../../shared/widget/share.dart';

class QuizBisaHomeScreen extends StatefulWidget {
  const QuizBisaHomeScreen({super.key});

  @override
  State<QuizBisaHomeScreen> createState() => _QuizBisaHomeScreenState();
}

class _QuizBisaHomeScreenState extends State<QuizBisaHomeScreen> {
  bool isClick = true;
  final List<bool> _bookmarks = List.filled(10, false);
  List<Map<String, dynamic>> dummyData = Quizdata.getListData();
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Quiz Terbaru dari BISA !",
            style: Styles.txt16medium,
          ),
          SizedBox(
            height: 16,
          ),
          Row(
            children: [
              Expanded(
                child: Container(
                  height: 180,
                  child: ListView.builder(
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      itemCount: Quizdata.getListData().length,
                      itemBuilder: (context, index) {
                        return Stack(
                          children: [
                            Container(
                              margin: EdgeInsets.only(right: 16),
                              width: 170,
                              decoration: BoxDecoration(
                                color: Styles.white,
                                borderRadius: BorderRadius.circular(12),
                                boxShadow: [
                                  BoxShadow(
                                      color: Color.fromRGBO(0, 0, 0, 0.3),
                                      offset: Offset(0, 4),
                                      blurRadius: 4)
                                ],
                              ),
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      height: 100,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                            bottomLeft: Radius.circular(12),
                                            bottomRight: Radius.circular(12)),
                                        image: DecorationImage(
                                            image: AssetImage(
                                                "${dummyData[index]['image']}"),
                                            fit: BoxFit.cover),
                                        boxShadow: [
                                          BoxShadow(
                                            color:
                                                Colors.black.withOpacity(0.25),
                                            spreadRadius: 0,
                                            blurRadius: 8,
                                            offset: Offset(0,
                                                2), // changes position of shadow
                                          ),
                                        ],
                                      ),
                                      child: Container(
                                        padding:
                                            EdgeInsets.only(left: 8, bottom: 8),
                                        width: double.infinity,
                                        alignment: Alignment.bottomLeft,
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.only(
                                                bottomLeft: Radius.circular(12),
                                                bottomRight:
                                                    Radius.circular(12)),
                                            gradient: LinearGradient(
                                                begin: Alignment.bottomCenter,
                                                end: Alignment.topCenter,
                                                stops: [
                                                  0.09,
                                                  0.5
                                                ],
                                                colors: [
                                                  Styles.black.withOpacity(1),
                                                  Styles.black.withOpacity(0.0),
                                                ])),
                                        child: Row(
                                          children: [
                                            Icon(
                                              Icons.map,
                                              color: Styles.white,
                                              size: 14,
                                            ),
                                            Gap(4),
                                            Text(
                                              "${dummyData[index]['title']}",
                                              style: Styles.txt14semibold
                                                  .copyWith(
                                                      color: Styles.white),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Gap(10),
                                    Padding(
                                      padding: EdgeInsets.only(left: 8.0),
                                      child: Text(
                                        "${dummyData[index]['status']}",
                                        style: Styles.txt14semibold,
                                      ),
                                    ),
                                    Gap(0),
                                    Padding(
                                      padding: EdgeInsets.only(left: 8.0),
                                      child: Text(
                                        "${dummyData[index]['soal']}",
                                        style: Styles.txt10medium.copyWith(
                                            color: Styles.grey, fontSize: 10),
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(left: 6.0),
                                      child: Row(
                                        children: [
                                          shareWidget().FiveStar(16),
                                          Text(
                                            "${dummyData[index]['rating']}",
                                            style: Styles.txt10medium
                                                .copyWith(color: Styles.grey),
                                          )
                                        ],
                                      ),
                                    ),
                                  ]),
                            ),
                            Positioned(
                              top: 85,
                              left: 115,
                              child: Container(
                                  width: 40,
                                  height: 40,
                                  decoration: BoxDecoration(
                                      color: Styles.white,
                                      borderRadius: BorderRadius.circular(5),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.black.withOpacity(0.25),
                                          spreadRadius: 0,
                                          blurRadius: 4,
                                          offset: Offset(0,
                                              4), // changes position of shadow
                                        ),
                                      ]),
                                  child: GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        _bookmarks[index] = !_bookmarks[
                                            index]; // Toggle bookmark state
                                      });
                                    },
                                    child: Icon(
                                      _bookmarks[index]
                                          ? Icons.bookmark
                                          : Icons.bookmark_border_rounded,
                                      color: Styles.darkBlue,
                                      size: 32,
                                    ),
                                  )),
                            ),
                          ],
                        );
                      }),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
