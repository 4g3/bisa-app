import 'package:bisa_release/screen/home_screen/components/ads/ads_home.dart';
import 'package:bisa_release/screen/home_screen/components/highlight_section.dart';
import 'package:bisa_release/screen/home_screen/components/pekerjaan_section.dart';
import 'package:bisa_release/screen/home_screen/components/quiz_bisa_section.dart';
import 'package:bisa_release/screen/home_screen/components/rekomendasi_ahli_section.dart';
import 'package:bisa_release/screen/home_screen/components/sliver_appbar_section.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';

class HomeScreen extends StatefulWidget {
  static String routeName = "/HomeScreen";
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: Scaffold(
          resizeToAvoidBottomInset: true,
          body: SafeArea(
            child: NestedScrollView(
                headerSliverBuilder:
                    (BuildContext context, bool innerBoxIsScrolled) {
                  return <Widget>[
                    SliverAppbarHomeScreen(),
                  ];
                },
                body: SingleChildScrollView(
                  physics: BouncingScrollPhysics(),
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 20),
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16.0),
                          child: HighlightHomeScreen(),
                        ),
                        Gap(18),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16.0),
                          child: PekerjaanHomeScreen(),
                        ),
                        Gap(16),
                        RekomendasiParaAhliHomeScreen(),
                        Gap(10),
                        QuizBisaHomeScreen(),
                        Gap(24),
                        Container(
                          child: Image.asset("assets/images/ads1.png"),
                        ),
                        Gap(16),
                        ads_home()
                      ],
                    ),
                  ),
                )),
          )),
    );
  }
}
