// ignore_for_file: unused_import

import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:bisa_release/model/storyModel.dart';

class apiService {
  final baseUrl = "https://bisatrackapp.com/api/";

  Future getstory({required String token}) async {
    final url1 = 'https://bisatrackapp.com/api/story';

    try {
      final response = await http.get(
        Uri.parse(url1),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          HttpHeaders.authorizationHeader: 'Bearer $token',
        },
      );
      print("get story");
      print('token : $token');
      print('status code : ${response.statusCode}');

      if (response.statusCode == 200) {
        print(url1);
        Story model = Story.fromJson(json.decode(response.body));
        print(response.body);
        return model;
      } else {
        throw Exception("Failed to fetch data from API");
      }
    } catch (e) {
      print(e.toString());
    }
  }
}
