import 'package:bisa_release/screen/load_screen/load_screen.dart';
import 'package:bisa_release/screen/register_screen/register_screen.dart';
import 'package:bisa_release/shared/widget/bottom_nav.dart';
import 'package:flutter/material.dart';

void main() {
  // SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.fullscreen]);

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'BISA',
      theme: ThemeData(
          // Your app theme data
          ),
      initialRoute: '/load',
      routes: {
        '/load': (context) => load_Screen(),
        '/register': (context) => RegisterScreen(),
        '/home': (context) => BottomNavigationBisaApp(),
      },
    );
  }
}
