import 'package:flutter/material.dart';

import '../style/styles.dart';

class shareWidget {
  Widget Star(double size) {
    return Icon(
      Icons.star_rate_rounded,
      color: Color(0xFFEFBC2F),
      size: size,
    );
  }

  Widget FiveStar(double size) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [Star(size), Star(size), Star(size), Star(size), Star(size)],
    );
  }

  Widget TextFieldLoginRegister(TextEditingController controller,
      String hintText, String labelText, bool obscureText) {
    return TextField(
      controller: controller,
      obscureText: obscureText,
      decoration: InputDecoration(
        hintText: hintText,
        label: Text(labelText),
        labelStyle: Styles.txt20medium.copyWith(color: Styles.primaryColor),
        hintStyle:
            Styles.txt16medium.copyWith(fontSize: 18, color: Styles.grey),
      ),
      style: Styles.txt16medium.copyWith(fontSize: 18),
    );
  }
}
