import 'package:bisa_release/screen/explore_screen/explore_screen.dart';
import 'package:bisa_release/screen/saved_screen/saved_screen.dart';
import 'package:bisa_release/screen/setting_temp/setting.dart';
import 'package:flutter/material.dart';

import '../../screen/home_screen/home_screen.dart';
import '../style/styles.dart';

class BottomNavigationBisaApp extends StatefulWidget {
  const BottomNavigationBisaApp({super.key});

  @override
  State<BottomNavigationBisaApp> createState() =>
      _BottomNavigationBisaAppState();
}

class _BottomNavigationBisaAppState extends State<BottomNavigationBisaApp> {
  int _selectedNavbar = 0;
  static const List<Widget> _widgetOptions = <Widget>[
    HomeScreen(),
    ExploreScreen(),
    saved_screen(),
    setting()
  ];

  void _changeSelectedNavBar(int index) {
    setState(() {
      _selectedNavbar = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _widgetOptions.elementAt(_selectedNavbar),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        showSelectedLabels: true,
        showUnselectedLabels: true,
        unselectedLabelStyle: Styles.txt12medium,
        selectedLabelStyle: Styles.txt12semibold,
        currentIndex: _selectedNavbar,
        iconSize: 22,
        backgroundColor: Styles.white,
        selectedItemColor: Styles.primaryColor,
        unselectedItemColor: Styles.grey,
        onTap: _changeSelectedNavBar,
        items: <BottomNavigationBarItem>[
          navItem('assets/images/icon/home.png', "Home"),
          navItem('assets/images/icon/explore.png', "Explore"),
          navItem('assets/images/icon/saved.png', "Saved"),
          navItem('assets/images/icon/setting.png', "Setting"),
        ],
      ),
    );
  }

  BottomNavigationBarItem navItem(String icon, String label) {
    return BottomNavigationBarItem(
      icon: ImageIcon(
        AssetImage(icon),
      ),
      label: label,
    );
  }
}
