import 'dart:math';

class Quizdata {
  static List<Map<String, dynamic>>? _data;
  static List<bool> _bookmarks = List.filled(10, false);

  static List<Map<String, dynamic>> getListData() {
    if (_data == null) {
      final random = Random();
      final imagePaths = [
        'assets/images/DummyQuiz/Quiz1.png',
        'assets/images/DummyQuiz/Quiz2.png',
        'assets/images/DummyQuiz/Quiz3.png',
        'assets/images/DummyQuiz/Quiz4.png',
        'assets/images/DummyQuiz/Quiz5.png',
        'assets/images/DummyQuiz/Quiz6.png',
        'assets/images/DummyQuiz/Quiz7.png',
        'assets/images/DummyQuiz/Quiz8.png',
        'assets/images/DummyQuiz/Quiz9.png',
        'assets/images/DummyQuiz/Quiz1.png',
      ];

      final titles = [
        'MBTI test',
        'Psycometric test',
        'Academic test',
        'IQ test',
        'EQ test',
        'Personality test',
        'INFT Details',
        'BISA general Test',
        'Personality Test',
        'Narcissism test',
      ];

      _data = List.generate(10, (index) {
        final bool isOnline = random.nextBool();
        final double rating = 1 + random.nextDouble() * 4;
        final imagePathIndex = random.nextInt(imagePaths.length);
        final imagePath = imagePaths[imagePathIndex];
        final int numQuestions = random.nextInt(36) + 5;

        // Generate a random status
        final status = isOnline ? 'BISA Team' : 'BISA Team';

        // Retrieve the bookmark state for this item
        final bool bookmark = _bookmarks[index];

        final titleIndex = index % titles.length; // Reuse titles cyclically

        return {
          'image': imagePath,
          'title': titles[titleIndex],
          'status': status,
          'rating': rating.toStringAsFixed(1),
          'soal': '$numQuestions soal',
          'bookmark': bookmark,
        };
      });
    }

    return _data!;
  }

  // Method to get the bookmark state for a specific item
  static bool getBookmarkState(int index) {
    return _bookmarks[index];
  }
}
