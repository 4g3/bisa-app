import 'dart:math';

class ArticleData {
  static List<Map<String, dynamic>> getListData() {
    final random = Random();

    final titles = [
      'Lorem ipsum dolor sit amet',
      'Consectetur adipiscing elit',
      'Sed do eiusmod tempor',
      'Incididunt ut labore et dolore',
      'Magna aliqua',
      'Ut enim ad minim veniam',
      'Quis nostrud exercitation ullamco',
      'Laboris nisi ut aliquip ex ea commodo',
      'Duis aute irure dolor',
      'Excepteur sint occaecat cupidatat non proident',
    ];

    final authors = [
      'John Doe',
      'Jane Smith',
      'Alex Johnson',
      'Emily Brown',
      'Michael Wilson',
    ];

    final imagePaths = [
      'assets/images/image1.png',
      'assets/images/image2.png',
      'assets/images/image3.png',
      // Add more image paths as needed
    ];

    return List.generate(10, (index) {
      final titleIndex = random.nextInt(titles.length);
      final authorIndex = random.nextInt(authors.length);
      final imagePathIndex = random.nextInt(imagePaths.length);
      final uploadHours = random.nextInt(24);

      return {
        'title': titles[titleIndex],
        'author': authors[authorIndex],
        'imagePath': imagePaths[imagePathIndex], // Add image path
        'uploadHours': '$uploadHours hours ago',
      };
    });
  }
}
