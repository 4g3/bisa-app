class TopSchoolData {
  static List<Map<String, dynamic>> getListData() {
    final imgPaths = [
      'assets/images/tops/img1.jpg',
      'assets/images/tops/img1.jpg',
      'assets/images/tops/img1.jpg',
      'assets/images/tops/img1.jpg',
      'assets/images/tops/img1.jpg',
      'assets/images/tops/img1.jpg',
      'assets/images/tops/img1.jpg',
      'assets/images/tops/img1.jpg',
      'assets/images/tops/img1.jpg',
      'assets/images/tops/img1.jpg',
      'assets/images/tops/img1.jpg',
    ];

    final schoolTitles = [
      'SMA Pradita Dirgantara',
      'MAN Insan Cendekia',
      'SMA Kolese Loyola',
      'SMA Unggulan CT Arsa foundation',
      'SMA IT Abnu Abbas',
      'SMA Pangudi Luhur Van Lith',
      'SMA 1 Magelang',
      'SMA 1 Semarang',
      'MAN 1 Kudus',
      'SMAN 03 Semarang',
    ];

    final schoolAddresses = [
      'Jl. Pemuda No. 123, Jakarta',
      'Jl. Merdeka No. 456, Surabaya',
      'Jl. Diponegoro No. 789, Bandung',
      'Jl. Sudirman No. 101, Yogyakarta',
      'Jl. Gajah Mada No. 202, Semarang',
      'Jl. A. Yani No. 303, Medan',
      'Jl. Pattimura No. 404, Makassar',
      'Jl. Ahmad Yani No. 505, Palembang',
      'Jl. S. Parman No. 606, Denpasar',
      'Jl. Gatot Subroto No. 707, Balikpapan',
    ];

    final jurScOptions = ['RPL', 'DKV', 'ANIMASI'];

    final urls = [
      'https://www.google.com',
      'https://www.example.com',
      'https://www.school1.com',
      'https://www.school2.com',
      'https://www.school3.com',
      'https://www.school4.com',
      'https://www.school5.com',
      'https://www.school6.com',
      'https://www.school7.com',
      'https://www.school8.com',
    ];

    return List.generate(5, (index) {
      final imgPathIndex = index % imgPaths.length;
      final titleIndex = index % schoolTitles.length;
      final addressIndex = index % schoolAddresses.length;
      final jurScIndex = index % 3; // Fixed index for jurSc
      final jarakSc =
          ((index % 100) * 0.1 + 0.1).toStringAsFixed(1); // Fixed jarakSc
      final ratingSc = ((index % 50) * 0.1).toStringAsFixed(1);
      final urlScIndex = index % urls.length;

      return {
        'imgSc': imgPaths[imgPathIndex],
        'titleSc': schoolTitles[titleIndex],
        'addressSc': schoolAddresses[addressIndex],
        'jarakSc': '$jarakSc km',
        'jurSc': [
          jurScOptions[jurScIndex],
          jurScOptions[(jurScIndex + 1) % 3],
          jurScOptions[(jurScIndex + 2) % 3],
        ],
        'ratingSc': ratingSc,
        'urlSc': urls[urlScIndex],
      };
    });
  }
}
