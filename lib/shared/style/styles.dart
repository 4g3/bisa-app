import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Styles {
  static Color white = const Color(0xFFFFFFFF);
  static Color black = const Color(0xFF000000);
  static Color grey = const Color(0xFF828282);
  static Color primaryColor = const Color(0xFF1CA0E3);
  static Color darkBlue = const Color(0xFF0D337C);

  static TextStyle appbarTitle = GoogleFonts.poppins(
    fontSize: 20,
    fontWeight: FontWeight.w600,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt61thin = GoogleFonts.poppins(
    fontSize: 61,
    fontWeight: FontWeight.w100,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt61extralight = GoogleFonts.poppins(
    fontSize: 61,
    fontWeight: FontWeight.w200,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt61light = GoogleFonts.poppins(
    fontSize: 61,
    fontWeight: FontWeight.w300,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt61regular = GoogleFonts.poppins(
    fontSize: 61,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt61medium = GoogleFonts.poppins(
    fontSize: 61,
    fontWeight: FontWeight.w500,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt61semibold = GoogleFonts.poppins(
    fontSize: 61,
    fontWeight: FontWeight.w600,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt61bold = GoogleFonts.poppins(
    fontSize: 61,
    fontWeight: FontWeight.w700,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt61extrabold = GoogleFonts.poppins(
    fontSize: 61,
    fontWeight: FontWeight.w800,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt61black = GoogleFonts.poppins(
    fontSize: 61,
    fontWeight: FontWeight.w900,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );

  static TextStyle txt49thin = GoogleFonts.poppins(
    fontSize: 49,
    fontWeight: FontWeight.w100,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt49extralight = GoogleFonts.poppins(
    fontSize: 49,
    fontWeight: FontWeight.w200,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt49light = GoogleFonts.poppins(
    fontSize: 49,
    fontWeight: FontWeight.w300,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt49regular = GoogleFonts.poppins(
    fontSize: 49,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt49medium = GoogleFonts.poppins(
    fontSize: 49,
    fontWeight: FontWeight.w500,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt49semibold = GoogleFonts.poppins(
    fontSize: 49,
    fontWeight: FontWeight.w600,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt49bold = GoogleFonts.poppins(
    fontSize: 49,
    fontWeight: FontWeight.w700,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt49extrabold = GoogleFonts.poppins(
    fontSize: 49,
    fontWeight: FontWeight.w800,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt49black = GoogleFonts.poppins(
    fontSize: 49,
    fontWeight: FontWeight.w900,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );

  static TextStyle txt39thin = GoogleFonts.poppins(
    fontSize: 39,
    fontWeight: FontWeight.w100,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt39extralight = GoogleFonts.poppins(
    fontSize: 39,
    fontWeight: FontWeight.w200,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt39light = GoogleFonts.poppins(
    fontSize: 39,
    fontWeight: FontWeight.w300,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt39regular = GoogleFonts.poppins(
    fontSize: 39,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt39medium = GoogleFonts.poppins(
    fontSize: 39,
    fontWeight: FontWeight.w500,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt39semibold = GoogleFonts.poppins(
    fontSize: 39,
    fontWeight: FontWeight.w600,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt39bold = GoogleFonts.poppins(
    fontSize: 39,
    fontWeight: FontWeight.w700,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt39extrabold = GoogleFonts.poppins(
    fontSize: 39,
    fontWeight: FontWeight.w800,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt39black = GoogleFonts.poppins(
    fontSize: 39,
    fontWeight: FontWeight.w900,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );

  static TextStyle txt31thin = GoogleFonts.poppins(
    fontSize: 31,
    fontWeight: FontWeight.w100,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt31extralight = GoogleFonts.poppins(
    fontSize: 31,
    fontWeight: FontWeight.w200,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt31light = GoogleFonts.poppins(
    fontSize: 31,
    fontWeight: FontWeight.w300,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt31regular = GoogleFonts.poppins(
    fontSize: 31,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt31medium = GoogleFonts.poppins(
    fontSize: 31,
    fontWeight: FontWeight.w500,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt31semibold = GoogleFonts.poppins(
    fontSize: 31,
    fontWeight: FontWeight.w600,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt31bold = GoogleFonts.poppins(
    fontSize: 31,
    fontWeight: FontWeight.w700,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt31extrabold = GoogleFonts.poppins(
    fontSize: 31,
    fontWeight: FontWeight.w800,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt31black = GoogleFonts.poppins(
    fontSize: 31,
    fontWeight: FontWeight.w900,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );

  static TextStyle txt24thin = GoogleFonts.poppins(
    fontSize: 24,
    fontWeight: FontWeight.w100,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt24extralight = GoogleFonts.poppins(
    fontSize: 24,
    fontWeight: FontWeight.w200,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt24light = GoogleFonts.poppins(
    fontSize: 24,
    fontWeight: FontWeight.w300,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt24regular = GoogleFonts.poppins(
    fontSize: 24,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt24medium = GoogleFonts.poppins(
    fontSize: 24,
    fontWeight: FontWeight.w500,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt24semibold = GoogleFonts.poppins(
    fontSize: 24,
    fontWeight: FontWeight.w600,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt24bold = GoogleFonts.poppins(
    fontSize: 24,
    fontWeight: FontWeight.w700,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt24extrabold = GoogleFonts.poppins(
    fontSize: 24,
    fontWeight: FontWeight.w800,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt24black = GoogleFonts.poppins(
    fontSize: 24,
    fontWeight: FontWeight.w900,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );

  static TextStyle txt20thin = GoogleFonts.poppins(
    fontSize: 20,
    fontWeight: FontWeight.w100,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt20extralight = GoogleFonts.poppins(
    fontSize: 20,
    fontWeight: FontWeight.w200,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt20light = GoogleFonts.poppins(
    fontSize: 20,
    fontWeight: FontWeight.w300,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt20regular = GoogleFonts.poppins(
    fontSize: 20,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt20medium = GoogleFonts.poppins(
    fontSize: 20,
    fontWeight: FontWeight.w500,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt20semibold = GoogleFonts.poppins(
    fontSize: 20,
    fontWeight: FontWeight.w600,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt20bold = GoogleFonts.poppins(
    fontSize: 20,
    fontWeight: FontWeight.w700,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt20extrabold = GoogleFonts.poppins(
    fontSize: 20,
    fontWeight: FontWeight.w800,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt20black = GoogleFonts.poppins(
    fontSize: 20,
    fontWeight: FontWeight.w900,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );

  static TextStyle txt16thin = GoogleFonts.poppins(
    fontSize: 16,
    fontWeight: FontWeight.w100,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt16extralight = GoogleFonts.poppins(
    fontSize: 16,
    fontWeight: FontWeight.w200,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt16light = GoogleFonts.poppins(
    fontSize: 16,
    fontWeight: FontWeight.w300,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt16regular = GoogleFonts.poppins(
    fontSize: 16,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt16medium = GoogleFonts.poppins(
    fontSize: 16,
    fontWeight: FontWeight.w500,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt16semibold = GoogleFonts.poppins(
    fontSize: 16,
    fontWeight: FontWeight.w600,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt16bold = GoogleFonts.poppins(
    fontSize: 16,
    fontWeight: FontWeight.w700,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt16extrabold = GoogleFonts.poppins(
    fontSize: 16,
    fontWeight: FontWeight.w800,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt16black = GoogleFonts.poppins(
    fontSize: 16,
    fontWeight: FontWeight.w900,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );

  static TextStyle txt14thin = GoogleFonts.poppins(
    fontSize: 14,
    fontWeight: FontWeight.w100,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt14extralight = GoogleFonts.poppins(
    fontSize: 14,
    fontWeight: FontWeight.w200,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt14light = GoogleFonts.poppins(
    fontSize: 14,
    fontWeight: FontWeight.w300,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt14regular = GoogleFonts.poppins(
    fontSize: 14,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt14medium = GoogleFonts.poppins(
    fontSize: 14,
    fontWeight: FontWeight.w500,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt14semibold = GoogleFonts.poppins(
    fontSize: 14,
    fontWeight: FontWeight.w600,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt14bold = GoogleFonts.poppins(
    fontSize: 14,
    fontWeight: FontWeight.w700,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt14extrabold = GoogleFonts.poppins(
    fontSize: 14,
    fontWeight: FontWeight.w800,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt14black = GoogleFonts.poppins(
    fontSize: 14,
    fontWeight: FontWeight.w900,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );

  static TextStyle txt12thin = GoogleFonts.poppins(
    fontSize: 12,
    fontWeight: FontWeight.w100,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt12extralight = GoogleFonts.poppins(
    fontSize: 12,
    fontWeight: FontWeight.w200,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt12light = GoogleFonts.poppins(
    fontSize: 12,
    fontWeight: FontWeight.w300,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt12regular = GoogleFonts.poppins(
    fontSize: 12,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt12medium = GoogleFonts.poppins(
    fontSize: 12,
    fontWeight: FontWeight.w500,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt12semibold = GoogleFonts.poppins(
    fontSize: 12,
    fontWeight: FontWeight.w600,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt12bold = GoogleFonts.poppins(
    fontSize: 12,
    fontWeight: FontWeight.w700,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt12extrabold = GoogleFonts.poppins(
    fontSize: 12,
    fontWeight: FontWeight.w800,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt12black = GoogleFonts.poppins(
    fontSize: 12,
    fontWeight: FontWeight.w900,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );

  static TextStyle txt10thin = GoogleFonts.poppins(
    fontSize: 10,
    fontWeight: FontWeight.w100,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt10extralight = GoogleFonts.poppins(
    fontSize: 10,
    fontWeight: FontWeight.w200,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt10light = GoogleFonts.poppins(
    fontSize: 10,
    fontWeight: FontWeight.w300,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt10regular = GoogleFonts.poppins(
    fontSize: 10,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt10medium = GoogleFonts.poppins(
    fontSize: 10,
    fontWeight: FontWeight.w500,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt10semibold = GoogleFonts.poppins(
    fontSize: 10,
    fontWeight: FontWeight.w600,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt10bold = GoogleFonts.poppins(
    fontSize: 10,
    fontWeight: FontWeight.w700,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt10extrabold = GoogleFonts.poppins(
    fontSize: 10,
    fontWeight: FontWeight.w800,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt10black = GoogleFonts.poppins(
    fontSize: 10,
    fontWeight: FontWeight.w900,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );

  static TextStyle txt8thin = GoogleFonts.poppins(
    fontSize: 8,
    fontWeight: FontWeight.w100,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt8extralight = GoogleFonts.poppins(
    fontSize: 8,
    fontWeight: FontWeight.w200,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt8light = GoogleFonts.poppins(
    fontSize: 8,
    fontWeight: FontWeight.w300,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt8regular = GoogleFonts.poppins(
    fontSize: 8,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt8medium = GoogleFonts.poppins(
    fontSize: 8,
    fontWeight: FontWeight.w500,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt8semibold = GoogleFonts.poppins(
    fontSize: 8,
    fontWeight: FontWeight.w600,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt8bold = GoogleFonts.poppins(
    fontSize: 8,
    fontWeight: FontWeight.w700,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt8extrabold = GoogleFonts.poppins(
    fontSize: 8,
    fontWeight: FontWeight.w800,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt8black = GoogleFonts.poppins(
    fontSize: 8,
    fontWeight: FontWeight.w900,
    fontStyle: FontStyle.normal,
    color: Styles.black,
  );
  static TextStyle txt4black = GoogleFonts.poppins(
    fontSize: 4,
    fontWeight: FontWeight.w900,
    fontStyle: FontStyle.normal,
    color: Colors.black,
  );
}
