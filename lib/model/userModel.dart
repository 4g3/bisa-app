import 'dart:convert';

UserModel userModelFromJson(String str) => UserModel.fromJson(json.decode(str));

String userModelToJson(UserModel data) => json.encode(data.toJson());

class UserModel {
    bool success;
    String message;
    Data data;

    UserModel({
        required this.success,
        required this.message,
        required this.data,
    });

    factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
        success: json["success"],
        message: json["message"],
        data: Data.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "success": success,
        "message": message,
        "data": data.toJson(),
    };
}

class Data {
    String token;
    String email;
    String username;

    Data({
        required this.token,
        required this.email,
        required this.username,
    });

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        token: json["token"],
        
        email: json["email"],
        username: json["username"],
    );

    Map<String, dynamic> toJson() => {
        "token": token,
        
        "email": email,
        "username": username,
    };
    
}
