class Questions {
  bool? success;
  String? message;
  List<Data>? data;

  Questions({this.success, this.message, this.data});

  Questions.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    message = json['message'];
    if (json['data'] != null) {
      data = <Data>[];
      json['data'].forEach((v) {
        data!.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int? id;
  String? quizId;
  String? text;
  String? optionA;
  String? optionB;
  String? createdAt;
  String? updatedAt;

  Data(
      {this.id,
      this.quizId,
      this.text,
      this.optionA,
      this.optionB,
      this.createdAt,
      this.updatedAt});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    quizId = json['quiz_id'];
    text = json['text'];
    optionA = json['option_a'];
    optionB = json['option_b'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['quiz_id'] = this.quizId;
    data['text'] = this.text;
    data['option_a'] = this.optionA;
    data['option_b'] = this.optionB;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
