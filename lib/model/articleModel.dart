class Article {
  final String title;
  final String image;
  final String content;

  Article({
    required this.title,
    required this.image,
    required this.content,
  });

  factory Article.fromJson(Map<String, dynamic> json) {
    return Article(
      title: json['title'],
      image: json['image'],
      content: json['content'],
    );
  }

  List<dynamic> toJson() {
    return [
      this.title,
      this.image,
      this.content,
    ];
  }
}
